# Research artefact — Compact Storage of Data Streams in Mobile Devices

This repository holds the source code that was used to compute the results and figures presented in
the paper.

## Cite the paper

```text
Rémy Raes, Olivier Ruas, Adrien Luxey-Bitri, Romain Rouvoy.
Compact Storage of Data Streams in Mobile Devices.
DAIS'24 - 24th International Conference on Distributed Applications and Interoperable Systems, Jun 2024, Groningen, Netherlands.
⟨hal-04535716v2⟩
```

```bibtex
@inproceedings{raes:hal-04535716,
  TITLE = {{Compact Storage of Data Streams in Mobile Devices}},
  AUTHOR = {Raes, R{\'e}my and Ruas, Olivier and Luxey-Bitri, Adrien and Rouvoy, Romain},
  URL = {https://hal.science/hal-04535716},
  BOOKTITLE = {{DAIS'24 - 24th International Conference on Distributed Applications and Interoperable Systems}},
  ADDRESS = {Groningen, Netherlands},
  PUBLISHER = {{LNCS}},
  SERIES = {Proceedings of the 24th International Conference on Distributed Applications and Interoperable Systems (DAIS'24)},
  YEAR = {2024},
  MONTH = Jun,
  KEYWORDS = {Mobile ; Android ; Storage ; Time series ; Pervasive ; Ubiquitous ; Data},
  PDF = {https://hal.science/hal-04535716v2/file/FLI_TSDB.pdf},
  HAL_ID = {hal-04535716},
  HAL_VERSION = {v2},
}
```

## Example Apps

The following applications are provided as example and used to benchmark our library.
* [Accelerometer](./temporaldb/example): example application of the lib, stores the accelerometer values using FLI.
* [Benchmarking against SQL](./benchmarking_memory_space): application made for benchmarking FLI and SQLite for memory and space on basic example (insertion of random or constant values).
* [Benchmarking against SWAB et Greycat](./benchmarking_throughput): application which compares the number of insertions/reads of FLI versus SWAB and Greycat.
* [In-situ LPPM](./in_situ_lppm): application implementing POI-attack and Promesse protection.

Additional parameters (such as the tolerated error) can be provided in a map:
`Factory myModel = Factory(0, {'error':'0.001'});`

Please refer to each directory for detailed instructions.
