# benchmarking_memory_space

Application made for benchmarking FLI and SQLite for memory and space on basic example.
The application is working on both Android and iOS.

## Purpose of this application

This app has four main buttons, one for each following experiment:
- Inserting random values in a SQLite database.
- Inserting random values in FLI.
- Inserting constant values in a SQLite database.
- Inserting constant values in FLI.

The last button, at the bottom right of the application, interrupts the current experiment.

#### Access result files

For each experiment, a file is created in the local directory of the app.
Every 10,000 insertions the size taken (on the disk) and the time spent are written in this file.
This value (10,000) is a parameter and can be changed in the implementation by changing the
value of the variable _stepMeasurement.

When the interrupt button is clicked, the experiment will be interrupted at the next measurement.

###### Android

For Android, files are located at:

`/storage/emulated/0/Android/data/fr.inria.spirals.temporaldb.benchmarking_memory_space/files/`

You can retrieve them using `adb` or Android Studio's device file explorer.

###### iOS

On iOS, you have to export data from application before reading it:

1. In Xcode, open `Window > Devices and Simulators`, and find tested application under `Installed apps`
   under your phone's menu
2. Export application container by selecting tested application and clicking `Download container` option
   in the menu below
3. `cd` your way to the `results` directory, which is located in `[YOUR_CONTAINER_NAME]/AppData/Library/Application\ Support`


## Getting Started

This project requires FLI, included in the package `temporaldb`.

Clone the `temporaldb` project and update its path in the pubspec.yaml file.
