import 'package:flutter_test/flutter_test.dart';
import 'package:temporaldb/epsilon_choice.dart';
import 'package:temporaldb/geo_example/utils.dart';
import 'package:tuple/tuple.dart';

void main() {
  List<Tuple2<double, double>> data = readHeartbeat();

  group('[Epsilon choice tests]', () {
    group('getDriftValue', () {
      test('should throw with an empty dataset', () {
        expect(
            () => getDriftValue([], 0.8),
            throwsA(predicate((e) =>
            e is ArgumentError &&
                e.message ==
                    'Cannot compute an empty dataset.')));
      });

      test('should throw with a negative threshold input', () {
        expect(
            () => getDriftValue(data, -1),
            throwsA(predicate((e) =>
                e is ArgumentError &&
                e.message ==
                    'Input threshold must be between 0 and 1 (was -1.0).')));
      });

      test('should throw with a big threshold input', () {
        expect(
                () => getDriftValue(data, 2),
            throwsA(predicate((e) =>
            e is ArgumentError &&
                e.message ==
                    'Input threshold must be between 0 and 1 (was 2.0).')));
      });

      test('should return median value', () {
        const List<Tuple2<double, double>> _data = [
          Tuple2(1, 10),
          Tuple2(2, 20),  // drift = 10
          Tuple2(3, 40),  // drift = 20
          Tuple2(4, 70)   // drift = 30
        ];

        double result = getDriftValue(_data, 0.5);
        expect(result, 20);
      });

      test('should return median value', () {
        const List<Tuple2<double, double>> _data = [
          Tuple2(1, 10),
          Tuple2(2, 20),  // drift = 10
          Tuple2(3, 40),  // drift = 20
          Tuple2(4, 70),  // drift = 30
          Tuple2(5, 110), // drift = 40
        ];

        double result = getDriftValue(_data, 0.5);
        expect(result, 25);
      });

      test('should return median value', () {
        const List<Tuple2<double, double>> _data = [
          Tuple2(1, 10),
          Tuple2(2, 20),  // drift = 10
          Tuple2(3, 40),  // drift = 20
          Tuple2(4, 70),  // drift = 30
          Tuple2(5, 110), // drift = 40
          Tuple2(6, 160), // drift = 50
        ];

        double result = getDriftValue(_data, 0.5);
        expect(result, 30);
      });

      test('should return first value', () {
        const List<Tuple2<double, double>> _data = [
          Tuple2(1, 10),
          Tuple2(2, 20),  // drift = 10
          Tuple2(3, 40),  // drift = 20
          Tuple2(4, 70),  // drift = 30
          Tuple2(5, 110), // drift = 40
        ];

        double result = getDriftValue(_data, 0);
        expect(result, 10);
      });

      test('should return last value', () {
        const List<Tuple2<double, double>> _data = [
          Tuple2(1, 10),
          Tuple2(2, 20),  // drift = 10
          Tuple2(3, 40),  // drift = 20
          Tuple2(4, 70),  // drift = 30
          Tuple2(5, 110), // drift = 40
        ];

        double result = getDriftValue(_data, 1);
        expect(result, 40);
      });

      test('should return last value', () {
        const List<Tuple2<double, double>> _data = [
          Tuple2(1, 10),
          Tuple2(2, 20),  // drift = 10
          Tuple2(3, 40),  // drift = 20
          Tuple2(4, 70),  // drift = 30
          Tuple2(5, 110), // drift = 40
          Tuple2(6, 160)  // drift = 50
        ];

        double result = getDriftValue(_data, 1);
        expect(result, 50);
      });
    });

    group('Dataset tests', () {
      test('should report a lower value with a lower threshold', () {
        double p80Value = getDriftValue(data, 0.8);
        double p90Value = getDriftValue(data, 0.9);
        expect(p80Value < p90Value, true);
      });
    });
  });
}
