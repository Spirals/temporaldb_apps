import 'dart:math';

import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:temporaldb/models/swab.dart';
import 'package:tuple/tuple.dart';

double error=0.001;
int nbPoints = 100;
double maxRandom = 100000;


bool arePolynomialsValid(SWAB swab, List<Tuple2<double,double>> points, double error) {
  double y;
  for(Tuple2<double,double> point in points) {
    y = swab.read(point.item1);
    if ((y-point.item2).abs() > error) {
      // swab.troubleShooting();
      debugPrint('Error reading ${point.item1}: expected ${point.item2}, got $y');
      debugPrint('Error ${(y-point.item2).abs()}');
      return false;
    }
  }
  return true;
}

List<Tuple2<double,double>> getPointsFromPolynomials(Tuple3<double,double,double> polynomial, double endPoint, int nbPoints) {
  List<Tuple2<double,double>> points = [];

  double step = (endPoint - polynomial.item1)/nbPoints;
  double x;
  double y;
  for(int i=0; i<nbPoints; i++) {
    x = polynomial.item1 + i * step;
    y = polynomial.item2 * x + polynomial.item3;
    points.add(Tuple2(x,y));
  }
  return points;
}

void testTopDownPieceWise(List<Tuple3<double,double,double>> polynomials) {
  test('testing SWAB piece-wise', ()
  {
    for(int i=0; i<polynomials.length-1; i++) {
      var points = getPointsFromPolynomials(polynomials[i], polynomials[i+1].item1, nbPoints);
      SWAB swab = SWAB();
      for (var point in points) {
        swab.add(point.item1, point.item2);
      }
      expect(arePolynomialsValid(swab, points, error),true);
    }
  });
}
void testTopDown(List<Tuple3<double,double,double>> polynomials) {
  test('testing TopDown', ()
  {
    List<Tuple2<double,double>> points = [];
    for(int i=0; i<polynomials.length-1; i++) {
      points += getPointsFromPolynomials(polynomials[i], polynomials[i+1].item1, nbPoints);
    }
    SWAB swab = SWAB();
    for (var point in points) {
      swab.add(point.item1, point.item2);
    }
    expect(arePolynomialsValid(swab, points, error),true);
  });
}

List<Tuple3<double,double,double>> generatePolynomials(int nbPolynomials, double step) {
  List<Tuple3<double,double,double>> polynomials = [];
  Random rd = Random(0);
  double startingTime=0;
  double A=0;
  double B=(rd.nextDouble()-1)*maxRandom;
  double newA;
  for(int indexPoly = 0; indexPoly < nbPolynomials; indexPoly++) {
    newA = (rd.nextDouble() - 1) * maxRandom;
    B = (A-newA) * startingTime + B;
    A = newA;
    startingTime += step;
    polynomials.add(Tuple3(startingTime,A,B));
  }
  return polynomials;
}

void main() {
  List<Tuple3<double,double,double>> polynomials = generatePolynomials(100, 100);
  testTopDownPieceWise(polynomials);
  testTopDown(polynomials);
}
