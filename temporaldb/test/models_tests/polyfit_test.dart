import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:scidart/numdart.dart';
import 'dart:math';


int maxPoint = 10;
int maxCoef = 100;

double getValuePoly(List<double> coeffList, double x) {
  double result = 0;
  for(int i=0; i<coeffList.length; i++) {
    result = coeffList[i] + x * result;
  }
  return result;
}

double getRandomDouble(Random rnd) {
//  return (rnd.nextInt(2 * 10) - 10 + rnd.nextDouble()).toDouble();
//  return double.parse((2*((rnd.nextDouble()-0.5)*maxPoint)).toStringAsFixed(2));
  return (2*((rnd.nextDouble()-0.5)*maxPoint));
}
double getRandomDoubleCoef(Random rnd) {
//  return (rnd.nextInt(2 * 10) - 10 + rnd.nextDouble()).toDouble();
//  return double.parse((2*((rnd.nextDouble()-0.5)*maxCoef)).toStringAsFixed(2));
  return (2*((rnd.nextDouble()-0.5)*maxCoef));
}

void main() {

  bool trainSet=true;
  bool testSet = true;


  var rnd =Random(1);

  PolyFit p;

  List<double> poly = [];
  List<double> listX = [];
  List<double> listY = [];
  List<double> listXTest = [];


  double y;
  double x;
  double coeff;

  double xTest;
  double yTest;

  int minDegree = 0;
  int maxDegree = 11;
  int nbPolyToTest=100;
  int nbPointToGeneratePoly=20;
  int nbPointToTest=50;

  double errorCoef = 0.001;


  test('Test polynomial All polynomial -> acceptValue()', () {

  /// generation values from 0 to 10 using polynomial
  for (int degree=minDegree;degree<=maxDegree;degree++){ // degree
    debugPrint('---------------degree '+degree.toString()+'--------------------');
    for (int iterPoly=0;iterPoly<nbPolyToTest;iterPoly++) { // number of polynomials to test
      debugPrint('----------degree: '+degree.toString()+'-----poly num: '+iterPoly.toString()+'--------------------');
      poly = [];
      listX = [];
      listY = [];
      y=0;
      for (int coeffDegree=0;coeffDegree<=degree;coeffDegree++) { // generation of polynomial
        //coeff=  rnd.nextInt(2*max)-max+rnd.nextDouble();
        coeff = getRandomDoubleCoef(rnd);
        poly.add(coeff);
      }
      debugPrint('---------------poly '+poly.toString()+'--------------------');

      while (listX.length<nbPointToGeneratePoly){ // generation of point
//        x=rnd.nextInt(2*max)-max+rnd.nextDouble();
        x = getRandomDouble(rnd);
        if(!listX.contains(x)){
          listX.add(x);
/*          y=0;
          for (int b=0;b<poly.length;b++){
            y+=poly[b]*pow(x,b);
          } */
          y = getValuePoly(poly,x);
          listY.add(y);
        }
      }


      p=PolyFit(Array(listX),Array(listY),degree);
      debugPrint('---------------PolyFit '+p.coefficients().toString()+'--------------------');
      if (trainSet) {
        for (int indexPts = 0; indexPts < nbPointToGeneratePoly; indexPts++) {
          xTest = listX[indexPts];
          yTest = listY[indexPts];
          //yTest = getValuePoly(poly,xTest);
          double yPredict = p.predict(xTest);
          double error_ = (yPredict - yTest).abs();
          if (error_ > yTest.abs() * errorCoef) {
            debugPrint(xTest as String?);
            debugPrint(yTest as String?);
            debugPrint(yPredict as String?);
            debugPrint(error_ as String?);
            debugPrint((yTest.abs() * errorCoef) as String?);
            debugPrint(p.coefficients() as String?);
            debugPrint(poly as String?);
          }
          expect(error_ <= yTest.abs() * errorCoef, true);
        }
      }

      if (testSet) {
        while (listXTest.length < nbPointToTest) { // Testing
//        xTest=rnd.nextInt(2*max)-max+rnd.nextDouble();
          xTest = getRandomDouble(rnd);
          if (!listX.contains(xTest) && !listXTest.contains(xTest)) {
            listXTest.add(xTest);
/*          yTest=0;
          for (int b=0;b<poly.length;b++){
            yTest+=poly[b]*pow(xTest,b);
          } */
            yTest = getValuePoly(poly, xTest);
            //print(yTest.toString()+" <-result with polynomial | result with Polyfit ->"+p.predict(xTest).toString());
//          expect(acceptValue(p,xTest,yTest,(yTest/1000).abs()), true);
            expect(
                (p.predict(xTest) - yTest).abs() <= yTest.abs() * errorCoef, true);
          }
        }
      }
    }
  }

  });

}