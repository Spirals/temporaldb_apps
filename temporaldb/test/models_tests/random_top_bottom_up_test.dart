import 'dart:math';

import 'package:flutter_test/flutter_test.dart';
import 'package:temporaldb/models/swab.dart';
import 'package:tuple/tuple.dart';

double error=0.001;
int nbPoints = 100;
double maxRandom = 100000;

double read(List<Tuple3<double,double,double>> polynomials, double x) {
  double y=double.nan;
  for (var poly in polynomials.reversed) {
    if (poly.item1 <= x) {
      return (poly.item2 * x + poly.item3);
    }
  }
  return y;
}

bool arePolynomialsValid(List<Tuple3<double,double,double>> polynomials, List<Tuple2<double,double>> points, double error) {
  double y;
  for(Tuple2<double,double> point in points) {
    y = read(polynomials,point.item1);
    if ((y-point.item2).abs() > error) {
      return false;
    }
  }
  return true;
}

List<Tuple2<double,double>> getPointsFromPolynomials(Tuple3<double,double,double> polynomial, double endPoint, int nbPoints) {
  List<Tuple2<double,double>> points = [];

  double step = (endPoint - polynomial.item1)/nbPoints;
  double x;
  double y;
  for(int i=0; i<nbPoints; i++) {
    x = polynomial.item1 + i * step;
    y = polynomial.item2 * x + polynomial.item3;
    points.add(Tuple2(x,y));
  }
  return points;
}

void testTopDownPieceWise(List<Tuple3<double,double,double>> polynomials) {
  test('testing TopDown piece-wise', ()
  {
    List<Tuple3<double,double,double>> polynomialsResults = [];
    for(int i=0; i<polynomials.length-1; i++) {
      var points = getPointsFromPolynomials(polynomials[i], polynomials[i+1].item1, nbPoints);
      polynomialsResults += SWAB.topDown(points, error);
      expect(arePolynomialsValid(polynomialsResults, points, error),true);
    }
  });
}
void testTopDown(List<Tuple3<double,double,double>> polynomials) {
  test('testing TopDown', ()
  {
    List<Tuple2<double,double>> points = [];
    List<Tuple3<double,double,double>> polynomialsResults;
    for(int i=0; i<polynomials.length-1; i++) {
      points += getPointsFromPolynomials(polynomials[i], polynomials[i+1].item1, nbPoints);
    }
    polynomialsResults = SWAB.topDown(points, error);
    expect(arePolynomialsValid(polynomialsResults, points, error),true);
  });
}

void testBottomUpPieceWise(List<Tuple3<double,double,double>> polynomials) {
  test('testing BottomUp piece-wise', ()
  {
    List<Tuple3<double,double,double>> polynomialsResults=[];
    for(int i=0; i<polynomials.length-1; i++) {
      var points = getPointsFromPolynomials(polynomials[i], polynomials[i+1].item1, nbPoints);
      polynomialsResults += SWAB.bottomUp(points, error);
      expect(arePolynomialsValid(polynomialsResults, points, error),true);
    }
  });
}
void testBottomUp(List<Tuple3<double,double,double>> polynomials) {
  test('testing BottomUp', ()
  {
    List<Tuple2<double,double>> points = [];
    List<Tuple3<double,double,double>> polynomialsResults;
    for(int i=0; i<polynomials.length-1; i++) {
      points += getPointsFromPolynomials(polynomials[i], polynomials[i+1].item1, nbPoints);
    }
    polynomialsResults = SWAB.bottomUp(points, error);
    expect(arePolynomialsValid(polynomialsResults, points, error),true);
  });
}

List<Tuple3<double,double,double>> generatePolynomials(int nbPolynomials, double step) {
  List<Tuple3<double,double,double>> polynomials = [];
  Random rd = Random(0);
  double startingTime=0;
  double A=0;
  double B=(rd.nextDouble()-1)*maxRandom;
  double newA;
  for(int indexPoly = 0; indexPoly < nbPolynomials; indexPoly++) {
    newA = (rd.nextDouble() - 1) * maxRandom;
    B = (A-newA) * startingTime + B;
    A = newA;
    startingTime += step;
    polynomials.add(Tuple3(startingTime,A,B));
  }
  return polynomials;
}

void main() {
  List<Tuple3<double,double,double>> polynomials = generatePolynomials(100, 100);
  testTopDownPieceWise(polynomials);
  testBottomUpPieceWise(polynomials);
  testTopDown(polynomials);
  testBottomUp(polynomials);
}
