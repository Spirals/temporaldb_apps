import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:scidart/numdart.dart';
import 'package:temporaldb/temporal_db_1d_factory.dart';
import 'package:temporaldb/models/temporal_model.dart';
import 'dart:math';

int typeModel = 5; /// 5 -> FLISW

double maxRandomPoint = 100.0;
int maxRandomCoef = 100;

double tnorm = pow(2,15).toDouble();
// double tnorm = 1000.0;
// double vnorm = 1.0;
double vnorm = pow(2,15).toDouble();


TemporalModel getModel(int type, double error, double tnorm, double ynorm) {
  Map<String,String> parameters = {
    'error':error.toString(),
    'timeValueNormalization':tnorm.toString(),
    'yValueNormalization':ynorm.toString(),
  };
  return (Factory(type,parameters));
}
TemporalModel getModelError(int type, double error) {
  Map<String,String> parameters = {
    'error':error.toString(),
  };
  return (Factory(type,parameters));
}

double getValuePoly(List<double> coeffList, double x) {
  double result = 0;
  for(int i=0; i<coeffList.length; i++) {
    result = coeffList[i] + x * result;
  }
  return result;
}

double getNext(double x) {
  return (x+100); //0.000001 ?
}

/// Time is necessarily positive
double getRandomDoubleTime(Random rnd) {
  return (rnd.nextDouble()*maxRandomPoint);
}

double getRandomDoubleCoef(Random rnd) {
//  return (rnd.nextDouble()*maxRandomPoint);
  return (2*((rnd.nextDouble()-0.5)*maxRandomCoef));
}
bool arePredictionCorrect(TemporalModel model, List<double> listX, List<double> listY, double error) {
  for (int i=0; i<listX.length; i++) {
    double t = listX[i];
    double v = listY[i];
    double prediction = model.read(t);
//    if(!((prediction-v).abs() <= v.abs()*error/(pow(2,model.getCurrentDegree()+1).toDouble()))){
    if(!((prediction-v).abs() <= v.abs()*error)){
      debugPrint('X values:');
      debugPrint(listX.toString());
      debugPrint('Y values:');
      debugPrint(listY.toString());
      debugPrint('WRONG PREDICTION: $t $v $prediction ${(prediction-v).abs()} ${(prediction-v).abs()/v.abs()}');
      return false;
    }
  }
  return true;
}



void main() {

  bool trainSet = true;
  bool testSet = false;

  int minDegree = 0;
  int maxDegree = 20;
  int nbPoly = 100;
  int nbPointsAdd = 200;
  int nbPointTests = 50;
  double errorRatio = 0.01;
  // double errorRatio = 0.00001;
  double errorRatioTest = 0.01;
  
  List<double> polynomialCoefficient;
  List<double> listX;
  List<double> listY;
  double x;
  double y;
  
  
  var rnd = Random(1);

  /// We make tests for each degree
  for(var degree = minDegree; degree <= maxDegree; degree++) {

    test('test the degree $degree', () {
      /// We tests several polynomials
      for (var iter = 0; iter < nbPoly; iter++) {
        /// We create a random polynomial of the current degree
        polynomialCoefficient = [];
        for (int coefNumber = 0; coefNumber <= degree; coefNumber++) {
          polynomialCoefficient.add(getRandomDoubleCoef(rnd));
        }

        TemporalModel model = getModel(typeModel,errorRatio,tnorm,vnorm);
//        model.updateYValueNormalization(maxRandomPoint);
//        model.updateYValueNormalization(vnorm);
//        model.updateTimeValueNormalization(tnorm);

//        debugPrint('starting adding points');
        ///We generate some points using the generated polynomial
        /// Those points will be used to generate our model
        listX = [];
        listY = [];
        x=0;
        while (listX.length<nbPointsAdd) {
          if (listX.isNotEmpty){
//            x = x+getRandomDoubleTime(rnd);
            x = getNext(x);
          }
          if(listX.contains(x)) {
            continue;
          }
          y = getValuePoly(polynomialCoefficient,x);
          listX.add(x);
          listY.add(y);
//          debugPrint('Adding point number $counter: $x, $y');
          model.add(x, y);
        }
//        debugPrint('Polynomial done: ${model.getNbModels()} model(s), last degree ${model.getCurrentDegree()}');
        debugPrint('Polynomial done: ${model.getNbModels()} model(s), last degree ${model.getModels().last.length-1}');

        /// Those points will be used to verify our model is correct
        if(trainSet) {
          bool isFitting = arePredictionCorrect(model, listX, listY, errorRatioTest);
          if(!isFitting) {
            debugPrint('Obtained model:');
            debugPrint('Last model out of the ${model.getNbModels()}:');
            debugPrint(model.getModels().last.toString());
//            model.printCurrentPoly();
            debugPrint('Expected polynomial:');
            debugPrint(polynomialCoefficient as String?);
            PolyFit p = PolyFit(Array(listX), Array(listY), degree);
            debugPrint('PolyFit polynomial:');
            debugPrint(p.coefficients().toString());
          }
          expect(isFitting, true);
        }
        /// We generate new points using the generated polynomial
        /// We test that the model generalizes on new data generated from the same polynomial
        if(testSet) {
          List<double> listXTest = [];
          List<double> listYTest = [];
          while (listXTest.length < nbPointTests) { // Testing
//            x = getRandomDoubleTime(rnd);
//            x = x + 0.000001;
            x = getNext(x);
            if (!listX.contains(x) && !listXTest.contains(x)) {
              listXTest.add(x);
              listYTest.add(getValuePoly(polynomialCoefficient, x));
            }
          }
          expect(arePredictionCorrect(model, listXTest, listYTest, errorRatio),true);
        }
        debugPrint('Degree $degree polynomial $iter OK');
        debugPrint('Total Size: ${model.getSize()}');

      }

    }); /// end of test

  }


}
