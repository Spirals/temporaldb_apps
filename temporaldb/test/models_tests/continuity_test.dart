import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:scidart/numdart.dart';
import 'package:temporaldb/temporal_db_1d_factory.dart';
import 'package:temporaldb/models/temporal_model.dart';


List<double> polynomCoeffs = [95.46726116652904, -41.22932499824357, 17.53111876212312, -81.78497030880662];
//[4214.0, 12552.0, 32123.0]; //[42.14,125.52,321.23];
int nbPointsAdd = 200;
double errorRatio = 0.01;
double errorRatioTest = 0.01;
double vNorm = pow(2,15).toDouble();
double tNorm = pow(2,15).toDouble();


double getValuePoly(List<double> coeffList, double x) {
  double result = 0;
  for(int i=0; i<coeffList.length; i++) {
    result = coeffList[i] + x * result;
  }
  return result;
}

double getMAEWithoutW0(List<double> coeff1, List<double> coeff2) {
  double mae = 0;
  for(var i=0; i<coeff1.length-1; i++) {
    mae = mae + (coeff1[i]-coeff2[i]).abs();
  }
  return mae;
}
double getRMSEWithoutC0(List<double> coeff1, List<double> coeff2) {
  double rmse = 0;
  double a;
  for(var i=0; i<coeff1.length-1; i++) {
    a=(coeff1[i]-coeff2[i]);
    rmse = rmse + a*a;
  }
  return sqrt(rmse);
}

double getMAE(List<double> coeff1, List<double> coeff2) {
  double mae = 0;
  for(var i=0; i<coeff1.length; i++) {
    mae = mae + (coeff1[i]-coeff2[i]).abs();
  }
  return mae;
}
double getRMSE(List<double> coeff1, List<double> coeff2) {
  double rmse = 0;
  double a;
  for(var i=0; i<coeff1.length; i++) {
    a=(coeff1[i]-coeff2[i]);
    rmse = rmse + a*a;
  }
  return sqrt(rmse);
}

bool arePredictionCorrect(TemporalModel model, List<double> listX, List<double> listY, double error) {
  for (int i=0; i<listX.length; i++) {
    double t = listX[i];
    double v = listY[i];
    double prediction = model.read(t);
    if(!((prediction-v).abs() <= v.abs()*error)){
      debugPrint('WRONG PREDICTION: $t $v $prediction ${(prediction-v).abs()} ${(prediction-v).abs()/v.abs()}');
      return false;
    }
  }
  return true;
}

TemporalModel getModel(int type, double error, double tnorm, double ynorm) {
  Map<String,String> parameters = {
    'error':error.toString(),
    'timeValueNormalization':tnorm.toString(),
    'yValueNormalization':ynorm.toString(),
  };
  return (Factory(type,parameters));
}
TemporalModel getModelError(int type, double error) {
  Map<String,String> parameters = {
    'error':error.toString(),
  };
  return (Factory(type,parameters));
}

List<double> getNorm(List<double> listRaw, double norm) {
  List<double> listNorm = [];
  for(double vRaw in listRaw) {
    listNorm.add(vRaw/norm);
  }
  return listNorm;
}



void testContinuity(int typeModel) {
  List<double> listT;
  List<double> listV;
  double t;
  double v;
  int nbModels;
  int degree;
  double mae;
  double rmse;
  List<double> coeff;
  TemporalModel model;
  model = Factory(typeModel);
  test('Continuity test for type ${model.getType()} t0=0 and no normalization', () {
//    model = getModel(typeModel, errorRatio, 1.0, 1.0);
    model = getModel(typeModel, errorRatio, tNorm, vNorm);
    listT = [];
    listV = [];
    t = 0;
    debugPrint('Polynom initialized');
    for (var i = 0; i < nbPointsAdd; i++) {
      v = getValuePoly(polynomCoeffs, t);
      listT.add(t);
      listV.add(v);
      // debugPrint('Adding point number $i: $t, $v');
      model.add(t, v);
      t = t + 470;

      /// Maximum value for which tests pass
/*        if(i % 20 == 0) {
          debugPrint('$i points added');
        }*/
    }
    debugPrint('Points added');
    nbModels = model.getNbModels();
//      degree = model.getCurrentDegree();
//      coeff = model.currentModel.coefficients().toList();
    coeff = model
        .getModels()
        .last;
    degree = coeff.length - 1;
    bool good = (nbModels == 1 && degree == polynomCoeffs.length - 1);
    if (good) {
      mae = getMAE(coeff, polynomCoeffs);
      rmse = getRMSE(coeff, polynomCoeffs);
      debugPrint('Polynom has the good degree :');
      debugPrint('MAE: $mae');
      debugPrint('RMSE: $rmse');
      debugPrint('Expected polynomCoeffs: ${polynomCoeffs
          .toString()}, obtained: ${coeff.toString()}');
      PolyFit p = PolyFit(Array(getNorm(listT,tNorm)), Array(getNorm(listV, vNorm)), degree);
      debugPrint('Normalized PolyFit polynom:');
      debugPrint(p.coefficients().toList().toString());
    }
    else {
      debugPrint('Error with degree or number of models');
      debugPrint('Number of models: $nbModels (should be 1)');
      debugPrint('Expected polynom: ${polynomCoeffs.toString()}');
      debugPrint('Degree: $degree (should be 2)');
      PolyFit p = PolyFit(Array(listT), Array(listV), degree);
      debugPrint('PolyFit polynom:');
      debugPrint(p.coefficients().toString());
      p = PolyFit(Array(getNorm(listT,tNorm)), Array(getNorm(listV, vNorm)), degree);
      debugPrint('Normalized PolyFit polynom:');
      debugPrint(p.coefficients().toString());
    }
    expect(good, true);

    /// We use the same points to verify our model is correct
    bool isFitting = arePredictionCorrect(
        model, listT, listV, errorRatioTest);
    expect(isFitting, true);
    debugPrint('Continuity test for ${model.getType()} OK');
  }); /// end of test
}








void main() {
  for (int typeModel=0; typeModel<3; typeModel++) {
    testContinuity(typeModel);
  }
}



