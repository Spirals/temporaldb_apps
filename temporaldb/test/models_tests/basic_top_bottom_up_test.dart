import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:temporaldb/models/swab.dart';
import 'package:tuple/tuple.dart';

/// The last polynomial should be only a x value: Tuple3(x,0,0)
List<Tuple3<double,double,double>> polynomials = [
  const Tuple3(0,1,0),
  // const Tuple3(100,0,0),
  const Tuple3(100,-1,200),
  const Tuple3(200,0,0),
  const Tuple3(300,0,0)
];
double error=0.001;
int nbPoints = 100;

double read(List<Tuple3<double,double,double>> polynomials, double x) {
  double y=double.nan;
  for (var poly in polynomials.reversed) {
    if (poly.item1 <= x) {
      return (poly.item2 * x + poly.item3);
    }
  }
  return y;
}

bool arePolynomialsValid(List<Tuple3<double,double,double>> polynomials, List<Tuple2<double,double>> points, double error) {
  double y;
  for(Tuple2<double,double> point in points) {
    y = read(polynomials,point.item1);
    if ((y-point.item2).abs() > error) {
      return false;
    }
  }
  return true;
}

List<Tuple2<double,double>> getPointsFromPolynomials(Tuple3<double,double,double> polynomial, double endPoint, int nbPoints) {
  List<Tuple2<double,double>> points = [];

  double step = (endPoint - polynomial.item1)/nbPoints;
  double x;
  double y;
  for(int i=0; i<nbPoints; i++) {
    x = polynomial.item1 + i * step;
    y = polynomial.item2 * x + polynomial.item3;
    points.add(Tuple2(x,y));
  }
  return points;
}

void testTopDownPieceWise() {
  test('testing TopDown piece-wise', ()
  {
    List<Tuple3<double,double,double>> polynomialsResults = [];
    for(int i=0; i<polynomials.length-1; i++) {
      var points = getPointsFromPolynomials(polynomials[i], polynomials[i+1].item1, nbPoints);
      polynomialsResults += SWAB.topDown(points, error);
      expect(arePolynomialsValid(polynomialsResults, points, error),true);
    }
    debugPrint(polynomialsResults.toString());
  });
}
void testTopDown() {
  test('testing TopDown', ()
  {
    List<Tuple2<double,double>> points = [];
    List<Tuple3<double,double,double>> polynomialsResults;
    for(int i=0; i<polynomials.length-1; i++) {
      points += getPointsFromPolynomials(polynomials[i], polynomials[i+1].item1, nbPoints);
    }
    polynomialsResults = SWAB.topDown(points, error);
    debugPrint(polynomialsResults.toString());
    expect(arePolynomialsValid(polynomialsResults, points, error),true);
  });
}

void testBottomUpPieceWise() {
  test('testing BottomUp piece-wise', ()
  {
    List<Tuple3<double,double,double>> polynomialsResults=[];
    for(int i=0; i<polynomials.length-1; i++) {
      var points = getPointsFromPolynomials(polynomials[i], polynomials[i+1].item1, nbPoints);
      polynomialsResults += SWAB.bottomUp(points, error);
      expect(arePolynomialsValid(polynomialsResults, points, error),true);
    }
    debugPrint(polynomialsResults.toString());
  });
}
void testBottomUp() {
  test('testing BottomUp', ()
  {
    List<Tuple2<double,double>> points = [];
    List<Tuple3<double,double,double>> polynomialsResults;
    for(int i=0; i<polynomials.length-1; i++) {
      points += getPointsFromPolynomials(polynomials[i], polynomials[i+1].item1, nbPoints);
    }
    polynomialsResults = SWAB.bottomUp(points, error);
    debugPrint(polynomialsResults.toString());
    expect(arePolynomialsValid(polynomialsResults, points, error),true);
  });
}

void main() {
  testTopDownPieceWise();
  testBottomUpPieceWise();
  testTopDown();
  testBottomUp();
}
