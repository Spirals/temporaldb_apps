import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:scidart/numdart.dart';


void main() {

  test('testing init polyfit', () {

    double error = 0.1;
    int degree = 0; /// Max degree (in practice, with nb_points = degree+1): 29
    int nbPoints=degree+10;

    double xTest = 10;
    double yTest = 0;

    List<double> listX = [];
    List<double> listY = [];

    for(int i=0; i < nbPoints; i++) {
      double x=i.toDouble();
      double y=i.toDouble();
      listX.add(x);
      listY.add(y);
    }
    debugPrint(listX.toString());
    debugPrint(listY.toString());
    debugPrint('points generated');
    //Model1D model = Model1D(0.1);
    PolyFit p = PolyFit(Array(listX), Array(listY), degree);

    debugPrint(p.degree as String?);
    debugPrint(p.coefficients().toString());
    debugPrint(p.coefficient(0).toString());
    debugPrint(p.predict(xTest) as String?);

    double error_ = (p.predict(xTest)-yTest).abs();

    assert(error_<error,true);

  }); /// end of test


}
