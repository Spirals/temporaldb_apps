import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:scidart/numdart.dart';
import 'dart:math';

///
///MAE < 1
///
/// WHEN ALL RANDOM SAME GENERATION
/// random: max 130 (implem 3)
/// Valid for degree from 0 to 7
///
/// random: max 1,000
/// Valid for degree from 0 to 4
///
/// random: max 100,000
/// Valid for degree from 0 to 2
/// WHEN DIFFERENT RANDOM
/// MaxRandomPoint = 100
/// random: max 1000
/// Valid for degree from 0 to 7
/// !!!!!!!!!!! MAE < 1 !!!!!!!! -> very small, especially for polynomials with large coef
///
///
/// CONCLUSION:
/// IF x.abs()<100 WE CAN USE POLYFIT WITH ACCURACY UP TO DEGREE 7
///
///

int maxRandom = 1000;
int maxRandomPoint = 100;
double maxMAE = 1;

double getValuePoly(List<double> coeffList, double x) {
  double result = 0;
  for(int i=0; i<coeffList.length; i++) {
    result = coeffList[i] + x * result;
  }
  return result;
}

double getRandomDouble(Random rnd) {
//  return (rnd.nextInt(2 * 100) - 100 + rnd.nextDouble()).toDouble();
//  return double.parse((2*((rnd.nextDouble()-0.5)*maxRandom)).toStringAsFixed(3));
  return (2*((rnd.nextDouble()-0.5)*maxRandom));
}

double getRandomDoublePoint(Random rnd) {
//  return (rnd.nextInt(2 * 100) - 100 + rnd.nextDouble()).toDouble();
//  return double.parse((2*((rnd.nextDouble()-0.5)*maxRandom)).toStringAsFixed(3));
  return (2*((rnd.nextDouble()-0.5)*maxRandomPoint));
}

double getMAE(List<double> listCoef, PolyFit p) {
  double mae=0;
  List<double> pCoeff = p.coefficients().toList();
  for(int degree = 0; degree < listCoef.length; degree++) {
    mae = mae + (pCoeff[degree]-listCoef[degree]).abs();
  }
  mae = mae / listCoef.length;
  return mae;
}


void main() {

  var rnd = Random(1);

  PolyFit p;

  List<double> poly = [];
  List<double> listX = [];
  List<double> listY = [];

  double y;
  double x;
  double coeff;

  int minDegree = 0;
  int maxDegree = 7;
  int nbPolyToTest=100;
  int nbPointToGeneratePoly=20;

  double mae;


  test('Test polynomial All polynomials -> acceptValue()', () {

 /// generation values from 0 to 10 using polynomial
  for (int degree=minDegree;degree<=maxDegree;degree++){ ///degree
    debugPrint('---------------degree '+degree.toString()+'--------------------');
    for (int iterPoly=0;iterPoly<nbPolyToTest;iterPoly++) { ///number of polynomials to test
      debugPrint('----------degree: '+degree.toString()+'-----poly num: '+iterPoly.toString()+'--------------------');
      poly = [];
      listX = [];
      listY = [];
      y=0;
      for (int coeffDegree=0;coeffDegree<=degree;coeffDegree++) { ///generation of polynomial
        coeff = getRandomDouble(rnd);
        poly.add(coeff);
      }
      debugPrint('---------------poly '+poly.toString()+'--------------------');

      while (listX.length<nbPointToGeneratePoly){ ///generation of point
        x = getRandomDoublePoint(rnd);
        if(!listX.contains(x)){
          listX.add(x);
          y = getValuePoly(poly,x);
          listY.add(y);
        }
      }


      p=PolyFit(Array(listX),Array(listY),degree);
      debugPrint('---------------PolyFit '+p.coefficients().toString()+'--------------------');
      mae = getMAE(poly, p);
      debugPrint('MAE: $mae');
      expect(mae < maxMAE, true);






    }
  }

  });

}