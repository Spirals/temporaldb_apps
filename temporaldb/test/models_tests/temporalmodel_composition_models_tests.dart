import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:temporaldb/temporal_db_1d_factory.dart';
import 'package:temporaldb/models/temporal_model.dart';


int typeModel = 5; /// 5 -> FLISW

double getValuePoly(List<double> coeffList, double x) {
  double result = 0;
  for(int i=0; i<coeffList.length; i++) {
    result = coeffList[i] + x * result;
  }
  return result;
}


bool arePredictionCorrect(TemporalModel model, List<double> listX, List<double> listY, double error) {
  for (int i=0; i<listX.length; i++) {
    double t = listX[i];
    double v = listY[i];
    double prediction = model.read(t);

    if(!((prediction-v).abs() <= v.abs()*error)){
      if(v != 0) {
        debugPrint('WRONG PREDICTION: $t $v $prediction ${(prediction-v).abs()} ${(prediction-v).abs()/v.abs()}');
      }
      else {
        if (prediction <= error) {
          continue;
        }
      }
      return false;
    }
  }
  return true;
}

TemporalModel getModel(int type, double error, double tnorm, double ynorm) {
  Map<String,String> parameters = {
    'error':error.toString(),
    'timeValueNormalization':tnorm.toString(),
    'yValueNormalization':ynorm.toString(),
  };
  return (Factory(type,parameters));
}
TemporalModel getModelError(int type, double error) {
  Map<String,String> parameters = {
    'error':error.toString(),
  };
  return (Factory(type,parameters));
}




void testComposition() {
  double t0=0;
  double step = 10;
  int nbPointsAddPerPolynomial = 2000;
  List<List<List<double>>> compositionList = [
    [[1,-t0],[nbPointsAddPerPolynomial*step],[-1,3*nbPointsAddPerPolynomial*step]],
//    [],
//    []
    ];
  double errorRatio = 0.01;
  double errorRatioTest = 0.01;
  List<double> listT;
  List<double> listV;
  double t;
  double v;
  int counter;
  int nbModels;
  List<int> degrees;
  List<double> timestamps;
  TemporalModel model;
  test('Basic test for composition', () {
    /// We tests several compositionList
    counter = 0;
    for (var polynomialsList in compositionList) {
      model = getModel(typeModel,errorRatio,1.0,1.0);
//      Greycat model = Greycat(errorRatio);
//      SketchDB model = SketchDB(errorRatio);
//      model.updateTimeValueNormalization(1.0);
//      model.updateYValueNormalization(1.0);
      listT = [];
      listV = [];
      t = t0;
      int i=0;
      for(var polynomial in polynomialsList) {
        debugPrint('Starting polynomial $i');
        for (var i = 0; i < nbPointsAddPerPolynomial; i++) {
          v = getValuePoly(polynomial, t);
          listT.add(t);
          listV.add(v);
          model.add(t, v);
          t = t + step;
        }
        i++;
      }
      var models = model.getModels();
      nbModels = model.getNbModels();
      timestamps = model.getTimestamps();
      degrees = []; //model.getDegrees();
      for (var listCoef in models) {
        if(listCoef.isEmpty) {
          degrees.add(0);
        }
        else {
          degrees.add(listCoef.length - 1);
        }
      }
      bool good = nbModels == 3;
      debugPrint('Results for ${model.getType()}:');
      if(good) {
        for (var index = 0; index < nbModels; index++) {
          debugPrint('Expected polynomial $index: ${polynomialsList[index].toString()}, polynomial obtained: ${models[index].toString()}');
          debugPrint('Associated timestamps: ${timestamps[index].toString()}');
          good = good && (models[index].length == polynomialsList[index].length);
        }
      }
      else {
        debugPrint('Error with degree or number of models');
        debugPrint('Number of models: $nbModels (should be 3)');
        debugPrint('Degree: ${degrees.toString()} (should be [1,0,1])');
        for(var listCoef in models) {
          debugPrint(listCoef.toString());
        }
      }
//      expect(good,true);

      /// We use the same points to verify our model is correct
      bool isFitting = arePredictionCorrect(
          model, listT, listV, errorRatioTest);
      expect(isFitting, true);
      debugPrint('Composition XP $counter OK');
      counter++;
    }
  }); /// end of test
}


void main() {
  testComposition();
}



