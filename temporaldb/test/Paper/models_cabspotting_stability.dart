import 'dart:io';
import 'dart:math';
import 'package:flutter_test/flutter_test.dart';
import 'package:temporaldb/geo_example/utils.dart';
import 'package:temporaldb/models/temporal_model.dart';
import 'package:temporaldb/temporal_db_1d_factory.dart';
import 'package:tuple/tuple.dart';


void testPOIExample(int user) {
  test('testing model stats on a single user', () {
    var dir = Directory.current.path+'/datasets/';
    // Map<int,List<Tuple3<double,double,double>>> gpsTraceDataset = readCSV(dir+'cabspotting');
    // String outputAdd = Directory.current.path+'/results/stability-cabspotting/';
    Map<int,List<Tuple3<double,double,double>>> gpsTraceDataset = readCSV(dir+'privamov-gps');
    String outputAdd = Directory.current.path+'/results/stability-privamov/';
    Directory(outputAdd).createSync(recursive: true);

    // List<Tuple3<double,double,double>> gpsTraceUser = [];
    TemporalModel lat;
    TemporalModel lng;
    List<double> timestamps = [];

    List<double> distributionNumberPerModel=[];
    List<double> distributionTimePerModel=[];

    gpsTraceDataset.forEach((user, gpsTraceUser) {
      lat = Factory(0, {'error':'0.001'});
      lng = Factory(0, {'error':'0.001'});
      for (var point in gpsTraceUser) { /// For PrivaMov
      // for (var point in gpsTraceUser.reversed) {
        /// reversed for cabspotting
        lat.add(point.item1, point.item2);
        lng.add(point.item1, point.item3);
        timestamps.add(point.item1);
      }

      var modelsLat = lat.getModelsAndTimestamps();
      var modelsLng = lng.getModelsAndTimestamps();
      List<double> numberPerModelLat = [];
      List<double> numberPerModelLng = [];
      List<double> maxTimePerModelLat = [];
      List<double> maxTimePerModelLng = [];

      for (int i=0; i<modelsLat.length; i++) {
        numberPerModelLat.add(0.0);
        maxTimePerModelLat.add(0.0);
      }
      for (int i=0; i<modelsLng.length; i++) {
        numberPerModelLng.add(0.0);
        maxTimePerModelLng.add(0.0);
      }

      for (var point in gpsTraceUser.reversed) {
        /// reversed for cabspotting
        double t = point.item1;

        int modelIndex=0;
        for (int i=0; i<modelsLat.length-1; i++) {
          if (modelsLat[i+1].item1 > t) {
            /// The good model is the previous one
            // debugPrint('${modelsLat[i+1].item1} $t $modelIndex');
            break;
          }
          modelIndex++;
        }
        if (modelsLat.isNotEmpty) {
          numberPerModelLat[modelIndex] = numberPerModelLat[modelIndex] + 1;
          maxTimePerModelLat[modelIndex] = max(
              maxTimePerModelLat[modelIndex], t - modelsLat[modelIndex].item1);
        }

        modelIndex=0;
        for (var i=0; i<modelsLng.length-1; i++) {
          if (modelsLng[i+1].item1 > t) {
            /// The good model is the previous one
            break;
          }
          modelIndex++;
        }
        // debugPrint('${numberPerModelLng.length} ${modelsLng.length} $modelIndex');
        if (modelsLng.isNotEmpty) {
          numberPerModelLng[modelIndex] = numberPerModelLng[modelIndex] + 1;
          maxTimePerModelLng[modelIndex] = max(
              maxTimePerModelLng[modelIndex], t - modelsLng[modelIndex].item1);
        }
      }

      distributionNumberPerModel.addAll(numberPerModelLat);
      distributionNumberPerModel.addAll(numberPerModelLng);
      distributionTimePerModel.addAll(maxTimePerModelLat);
      distributionTimePerModel.addAll(maxTimePerModelLng);

    });

    List<Tuple2<double, double>> distribNumber = cumulativeDistribution(distributionNumberPerModel);
    List<Tuple2<double, double>> distribTime = cumulativeDistribution(distributionTimePerModel);


    writeResult(outputAdd+'distributionNumberPerModel.txt', distribNumber.toString());
    writeResult(outputAdd+'distributionMaxTimePerModel.txt', distribTime.toString());

    expect(true,true);
  });
}




void main() {
  testPOIExample(0);
}