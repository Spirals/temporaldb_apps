import 'dart:io';
import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:temporaldb/geo_example/utils.dart';
import 'package:temporaldb/gps_db.dart';
import 'package:temporaldb/models/temporal_model.dart';
import 'package:temporaldb/temporal_db_1d_factory.dart';
import 'package:tuple/tuple.dart';
import 'package:collection/collection.dart';


/// Testing GPSDB on the artificial dataset
void testStoreCSVArtificialDataset() {
  test('storing the Artificial Dataset', ()
  {
    var dir = Directory.current.path+'/datasets/';

    List<Tuple3<double,double,double>> gpsTraceUser0 = readCSVUser(dir+'artificial_dataset.txt',0);
    GPSDB modeledTrace0 = GPSDB();
    List<double> timestamps=[];
    for(var tuple3 in gpsTraceUser0) {
      modeledTrace0.add(tuple3.item1, tuple3.item2, tuple3.item3);
      timestamps.add(tuple3.item1);
    }
    List<Tuple3<double,double,double>> trace0 = modeledTrace0.getTrace(timestamps);

    List<Tuple3<double,double,double>> gpsTraceUser1 = readCSVUser(dir+'artificial_dataset.txt',1);
    GPSDB modeledTrace1 = GPSDB();
    timestamps=[];
    for(var tuple3 in gpsTraceUser1) {
      modeledTrace1.add(tuple3.item1, tuple3.item2, tuple3.item3);
      timestamps.add(tuple3.item1);
    }
    List<Tuple3<double,double,double>> trace1 = modeledTrace1.getTrace(timestamps);


    Function eq = const ListEquality().equals;

    bool listEq0 = eq(trace0,gpsTraceUser0);
    bool listEq1 = eq(trace1,gpsTraceUser1);
    bool listEq = listEq0 && listEq1;

    // debugPrint('Size of raw trace 0: ${(3*gpsTraceUser0.length).toString()}');
    // debugPrint('Size of final model 0: ${modeledTrace0.getSize().toString()}');
    // debugPrint('Size of raw trace 1: ${(3*gpsTraceUser1.length).toString()}');
    // debugPrint('Size of final model 1: ${modeledTrace1.getSize().toString()}');
    expect(listEq,true);
  });
}

void testREAD() {
  test('testing dichotomy read', ()
  {
  TemporalModel model = Factory(0,{'error':'0.001'});
  for(int i=0; i<100; i++) {
    model.add(i.toDouble(),i/2 % 2);
  }
  double t;
  double v;
  debugPrint('nb de models: ${model.getNbModels()}');
  for(int i=0; i<100; i++) {
    t = i.toDouble();
    v = model.read(t);
    debugPrint('$t $v');
  }
  // model.add(0,0);
  // model.add(1,1);
  // model.add(2,1);
  // model.add(3,0);
  // debugPrint(model.read(0.5));
  // debugPrint(model.read(0.5));
  expect(true, true);
  });
}

void testTimestampModeling() {
  test('testing modeling timestamps on the Cabspotting dataset user 0', ()
  {
    /// Results for privamov-gps-user1 using an error of 1:
    /// Number of points: 4341716.0
    /// Number of models: 26862
    /// Size of model: 80592.0
    /// Gain: 98.14377541045982%
    /// mae: 0.246398716079986


    var dir = Directory.current.path + '/datasets/';
    List<Tuple3<double, double, double>> gpsTraceUser_ = readCSVUser(
        // dir + 'privamov-gps-user1-sampling10', 1);
        dir + 'privamov-gps-user1', 1);
        // dir + 'cabspotting', 1);
    List<double> timestampsUser = [];
    TemporalModel model = Factory(0, const {'error': '1'});
    int timestampIndex = 0;
    for (var point in gpsTraceUser_) {
    // for (var point in gpsTraceUser_.reversed) {
      timestampsUser.add(point.item1);
      model.add(timestampIndex.toDouble(), point.item1);
      timestampIndex++;
    }
    double mae = 0;
    List<double> modeledTimestamps = [];
    for (int index = 0; index < timestampIndex; index++) {
      modeledTimestamps.add(model.read(index.toDouble()));
      mae = mae + (modeledTimestamps[index] - timestampsUser[index]).abs();
    }
    mae = mae / timestampIndex;
    // debugPrint(timestampsUser.sublist(0,10).toString());
    // debugPrint(modeledTimestamps.sublist(0,10).toString());
//  debugPrint(model.getTimestamps().toString());
//  debugPrint(model.getModels().toString());

    // List<double> timestampsStep = [];
    // for(int i=1; i<timestampsUser.length; i++) {
    //   timestampsStep.add(timestampsUser[i]-timestampsUser[i-1]);
    // }
    // Stats stats = Stats(timestampsStep);

    double rawSize = timestampsUser.length.toDouble();
    double modelSize = model.getSize().toDouble();

    debugPrint('Number of points: $rawSize');
    debugPrint('Number of models: ${model
        .getModels()
        .length}');
    debugPrint('Size of model: $modelSize');
    debugPrint('Gain: ${(100*(rawSize-modelSize))/rawSize}%');
    // debugPrint('Distribution step values:');
    // debugPrint('min: ${stats.min} max: ${stats.max}');
    // debugPrint('average: ${stats.getMean()}');
    // // debugPrint('Frequencies:');
    // // debugPrint(stats.getFrecuencies()); // [[1, 3], [2, 2], [3, 3], [4, 3], [5, 3], [6, 4], [7, 2]]
    // debugPrint('median: ${stats.getMedian()}');
    // // debugPrint('variance: ${stats.getVariance()}');
    // debugPrint('standard deviation: ${stats.getStandardDeviation()}');
    // debugPrint('range: ${stats.getRange()}');
    debugPrint('mae: $mae');
    bool b = mae < 60;
    expect(b, true);
  });
}



void main() {
  // testStoreCSVArtificialDataset();
  testTimestampModeling();
  // testREAD();
}