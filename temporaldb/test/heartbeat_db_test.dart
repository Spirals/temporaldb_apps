import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:temporaldb/geo_example/utils.dart';
import 'package:temporaldb/models/temporal_model.dart';
import 'package:temporaldb/temporal_db_1d_factory.dart';
import 'package:tuple/tuple.dart';


void testStoreHeartbeatDataset() {
  test('Storing heartbeat dataset', () {
    List<Tuple2<double, double>> samples = readHeartbeat();
    TemporalModel db = Factory(0, {'error': '100'});
    List<double> timestamps = [];

    // Store all data in model
    for (Tuple2<double, double> sample in samples) {
      db.add(sample.item1, sample.item2);
      timestamps.add(sample.item1);
    }

    // Compare sizes
    int rawSize = 2 * samples.length; // timestamp + ppi value
    int modelSize = db.getSize();
    debugPrint('Size of raw data: $rawSize');
    debugPrint('Size of model: $modelSize');
    expect(modelSize < rawSize, true);
  });

  test('Compute heartbeat models metrics', () {
    List<Tuple2<double, double>> samples = readHeartbeat(useFullDataset: true);
    TemporalModel db = Factory(0, {'error': '100'});

    // Store all data in model
    for (Tuple2<double, double> sample in samples) {
      db.add(sample.item1, sample.item2);
    }

    // Compute metrics
    List<double> errors = [];
    for (Tuple2<double, double> sample in samples) {
      double sampleTimestamp = sample.item1;
      double error = (sample.item2 - db.read(sampleTimestamp)).abs();
      errors.add(error);
    }
    double meanSquaredError = errors
        .reduce((value, element) => value + element) / errors.length;

    // Results
    debugPrint('Size of raw data: ${2 * samples.length}');
    debugPrint('Size of model: ${db.getSize()}');
    debugPrint('Mean error: $meanSquaredError');
  });
}


void main() {
  testStoreHeartbeatDataset();
}