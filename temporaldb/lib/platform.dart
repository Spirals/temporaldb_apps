import 'dart:io';
import 'package:intl/intl.dart';
import 'package:path_provider/path_provider.dart';
import 'dart:math';

/// Returns a path where application can create stuff.
Future<String> get localPath async {
  final directory = await getApplicationSupportDirectory();
  return directory.path;
}

/// Returns a path where files can be created by the benchmarks.
/// This is used to write experiments results.
Future<File> localDirFile(String dir, String fileName) async {
  final path = await localPath;
  final d = Directory('$path/$dir');
  d.create(recursive: true);
  File file = File('${d.path}/$fileName');
  await file.create(recursive: true);
  return file;
}

/// Returns a formatted timestamp for the current time.
/// This is used to name results files in a unique way.
String getTimestamp() {
  var date = DateTime.now();
  String s = DateFormat('yyyy-MM-dd--HH-mm-ss').format(date);
  return s;
}

/// Returns a random double between -amplitude/2 and amplitude/2
/// The default value for amplitude is 2000.
double getNextDouble(Random rd, [double amplitude = 2000]) {
  return ((rd.nextDouble() - 0.5) * amplitude);
}

/// Returns a positive random double between 0 and amplitude
/// The default value for amplitude is 1000.
double getNextPositiveDouble(Random rd, [double amplitude = 000]) {
  return (rd.nextDouble() * amplitude);
}
