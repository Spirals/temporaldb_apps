library temporaldb;

import 'dart:io';
import 'package:tuple/tuple.dart';

/// Given a list of values, returns the cumulative distribution in the form of a
/// list of pairs (v, ratio) with ratio being the proportion of values lower or
/// equals to v.
List<Tuple2<double, double>> cumulativeDistribution(List<double> distribution) {
  List<Tuple2<double, double>> cumulativeDistributionAux = [];
  distribution.sort();
  double currentValue = double.nan;
  double nbItems = 0;
  for (var value in distribution) {
    if (value != currentValue) {
      cumulativeDistributionAux.add(Tuple2(currentValue, nbItems));
      currentValue = value;
    }
    nbItems++;
  }
  cumulativeDistributionAux.add(Tuple2(currentValue, nbItems));
  cumulativeDistributionAux.removeAt(0);

  /// Removing Tuple2(-1,0)
  List<Tuple2<double, double>> cumulativeDistribution = [];
  for (var tuple2 in cumulativeDistributionAux) {
    cumulativeDistribution.add(Tuple2(tuple2.item1, tuple2.item2 / nbItems));
  }
  return cumulativeDistribution;
}

/// Reads a file and return the gps trace stored in it.
///
/// It assumes a CSV file with a [delimiter] which can be personalized.
Map<int, List<Tuple3<double, double, double>>> readCSV(String filename,
    [String delimiter = ';']) {
  List<String> file = File(filename).readAsLinesSync();
  Map<int, List<Tuple3<double, double, double>>> gpsTrace = {};
  int user;
  double timestamp;
  double lat;
  double lng;
  List<Tuple3<double, double, double>> auxList;
  List<String> words;
  for (var line in file) {
    words = line.split(delimiter);
    user = int.parse(words[0]);
    timestamp = double.parse(words[3]);
    lat = double.parse(words[1]);
    lng = double.parse(words[2]);
    auxList = gpsTrace.putIfAbsent(user, () => []);
    auxList.add(Tuple3(timestamp, lat, lng));
    gpsTrace.update(user, (value) => auxList);
  }
  return gpsTrace;
}

/// Does the same stuff as `readCSV`, but only for a specific user.
List<Tuple3<double, double, double>> readCSVUser(String filename, int user,
    [String delimiter = ';']) {
  List<String> file = File(filename).readAsLinesSync();
  List<Tuple3<double, double, double>> gpsTrace = [];
  double timestamp;
  double lat;
  double lng;
  List<String> words;
  for (var line in file) {
    words = line.split(delimiter);
    if (user != int.parse(words[0])) {
      continue;
    }
    timestamp = double.parse(words[3]);
    lat = double.parse(words[1]);
    lng = double.parse(words[2]);
    gpsTrace.add(Tuple3(timestamp, lat, lng));
  }
  return gpsTrace;
}

List<Tuple2<double, double>> readHeartbeat({bool useFullDataset = false}) {
  String filename = Directory.current.path +
      '/datasets/heartbeat_pulse${useFullDataset ? '_full' : ''}.txt';
  List<Tuple2<double, double>> pulses = [];
  List<String> file = File(filename).readAsLinesSync();
  for (String line in file) {
    List<String> words = line.split(';');
    pulses.add(Tuple2(double.parse(words[0]), double.parse(words[1])));
  }
  return pulses;
}

void writeResult(String filename, String result) {
  var file = File(filename);
  file.writeAsStringSync(result);
}

void writeResultAsBytes(String filename, List<int> result) {
  var file = File(filename);
  file.writeAsBytesSync(result);
}
