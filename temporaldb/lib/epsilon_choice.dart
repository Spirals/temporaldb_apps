import 'package:tuple/tuple.dart';

/// Helps developers choosing an epsilon value from the raw modeled data (with
/// an epsilon value of 0).
///
///
/// The choice of an epsilon value is of major importance and plays a central
/// role in FLI’s performances: a poorly-chosen value will have a strong impact
/// on FLI’s underlying segments, either degrading modeled data quality or
/// filling storage space up excessively.
///
/// To find a compromise between the two, since the epsilon value is highly
/// correlated to the modeled data, one has to know the data; more specifically,
/// we advise studying data variation between consecutive values.
///
/// This function does this by computing a percentile over input [data]'s drift
/// values, observed between consecutive values (x1, y1) and (x2, y2) and
/// computed as |(y2 - y1) / (x2 - x1)|. The epsilon value covered by
/// [threshold] of [data] will then be returned: in other words, [threshold]%
/// of [data] report a drift lower than returned value.
///
double getDriftValue(List<Tuple2<double, double>> data, double threshold) {
  // Input checks
  if (data.isEmpty) {
    throw ArgumentError('Cannot compute an empty dataset.');
  }
  if (threshold < 0 || threshold > 1) {
    throw ArgumentError(
        'Input threshold must be between 0 and 1 (was $threshold).');
  }

  // Drift computation
  List<double> drifts = [];
  for (int i = 0; i < data.length - 1; i++) {
    Tuple2<double, double> current = data[i];
    Tuple2<double, double> next = data[i + 1];
    double drift =
        ((next.item2 - current.item2) / (next.item1 - current.item1)).abs();
    drifts.add(drift);
  }

  // Get quantile value
  // Reimplementation of https://stackoverflow.com/a/2753343
  drifts.sort();
  double k = (drifts.length - 1) * threshold;
  int f = k.floor();
  int c = k.ceil();
  if (f == c) {
    return drifts[k.toInt()];
  }
  double d0 = drifts[f] * (c - k);
  double d1 = drifts[c] * (k - f);
  return d0 + d1;
}
