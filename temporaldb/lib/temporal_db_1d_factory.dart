library temporaldb;

import 'package:temporaldb/models/sketching.dart';
import 'package:temporaldb/models/sketching_sliding_window.dart';
import 'package:temporaldb/models/fast_linear_interpolation.dart';
import 'package:temporaldb/models/fast_linear_interpolation_sliding_windows.dart';

import 'package:temporaldb/models/temporal_model.dart';
import 'package:temporaldb/models/greycat.dart';
import 'package:temporaldb/models/swab.dart';
import 'package:tuple/tuple.dart';

/// Factory is a generic [TemporalModel] factory, that can construct FLI,
/// Greycat, SWAB or more advanced models.
///
/// Additional parameters such as the error threshold or model-specific
/// parameters should be provided in a map, e.g. {'error':'0.001'}.
///
/// The default value for the error is 0.01.
///
/// Note that points have to be added in chronological order.
class Factory implements TemporalModel {
  late final TemporalModel M;

  /// Creates a temporal model, implementing the [TemporalModel] class.
  ///
  /// Default / 0: FLI
  /// 1: SWAB
  /// 2: Greycat
  /// 3: SketchDB (extension of greycat)
  /// 4: SketchDBSW (with sliding window)
  /// 5: FLISW (FLI with sliding window)
  ///
  /// The default value for the error is 0.01.
  Factory([int type = 0, Map<String, String>? map]) {
    switch (type) {
      case 0:
        {
          M = FastLinearInterpolation();
        }
        break;
      case 1:
        {
          M = SWAB();
        }
        break;
      case 2:
        {
          M = Greycat();
        }
        break;
      case 3:
        {
          M = SketchDB();
        }
        break;
      case 4:
        {
          M = SketchDBSW();
        }
        break;
      case 5:
        {
          M = FLISlidingWindow();
        }
        break;
      default:
        {
          M = FastLinearInterpolation();
        }
    }
    if (map != null) {
      M.setParameters(map);
    }
    return;
  }

  @override
  void add(double t_, double v_) {
    M.add(t_, v_);
  }

  @override
  double read(double t) {
    return M.read(t);
  }

  @override
  List<List<double>> getModels() {
    return M.getModels();
  }

  @override
  int getNbModels() {
    return M.getNbModels();
  }

  @override
  List<double> getTimestamps() {
    return M.getTimestamps();
  }

  @override
  int getSize() {
    return M.getSize();
  }

  @override
  void setParameters(Map<String, String> map) {
    M.setParameters(map);
  }

  @override
  String getType() {
    return M.getType();
  }

  @override
  List<Tuple2<double, List<double>>> getModelsAndTimestamps() {
    return M.getModelsAndTimestamps();
  }
}
