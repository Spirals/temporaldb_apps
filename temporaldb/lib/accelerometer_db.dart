library temporaldb;

import 'package:temporaldb/models/temporal_model.dart';
import 'package:temporaldb/temporal_db_1d_factory.dart';
import 'package:tuple/tuple.dart';

/// This class models accelerometer values.
///
/// It stores three temporal models ([TemporalModel] instances), one for each
/// value of the accelerometer. It does not store the timestamps. By default, it
/// uses FLI model with an error of 0.1.
///
/// Note that points have to be added in chronological order.
///
/// The accelerometer is expressed in m/s^2.
class AccelerometerDB {
  /// Accelerometer is expressed in m/s^2
  late final TemporalModel xM;
  late final TemporalModel yM;
  late final TemporalModel zM;

  double startingTime = -1;
  double currentTime = -1;
  int nbPoints = 0;

  /// Creates a model, default value: FLI with error of 1.
  AccelerometerDB(
      [int type = 0, Map<String, String>? map = const {'error': '1'}]) {
    xM = Factory(type, map);
    yM = Factory(type, map);
    zM = Factory(type, map);
    return;
  }

  /// Adds an accelerometer value ([x], [y], [z]) at time [t].
  void add(double t, double x, double y, double z) {
    if (startingTime == -1) {
      startingTime = t;
    }
    xM.add(t, x);
    yM.add(t, y);
    zM.add(t, z);
    nbPoints++;
    currentTime = t;
  }

  /// Returns the value at time [t].
  /// Throws an exception if the time is not defined in the model.
  Tuple3<double, double, double> read(double t) {
    return Tuple3(xM.read(t), yM.read(t), zM.read(t));
  }

  /// Returns the count of double/int values used by the model.
  int getSize() {
    return xM.getSize() + yM.getSize() + zM.getSize() + 3;
  }

  /// Returns the trace associated to the given [timestamps].
  List<Tuple4<double, double, double, double>> getTrace(
      List<double> timestamps) {
    List<Tuple4<double, double, double, double>> trace = [];
    Tuple3<double, double, double> point;
    for (double time in timestamps) {
      point = read(time);
      trace.add(Tuple4<double, double, double, double>(
          time, point.item1, point.item2, point.item3));
    }
    return trace;
  }

  /// Returns the trace associated to the timestamps of the models.
  List<Tuple4<double, double, double, double>> getTraceFromScratch() {
    if (nbPoints == 0) {
      return [];
    }
    Tuple3<double, double, double> point;
    if (nbPoints == 1) {
      point = read(startingTime);
      return [
        Tuple4<double, double, double, double>(
            startingTime, point.item1, point.item2, point.item3)
      ];
    }
    List<Tuple4<double, double, double, double>> trace = [];

    List<double> timestampsX = xM.getTimestamps();
    List<double> timestampsY = yM.getTimestamps();
    List<double> timestampsZ = zM.getTimestamps();
    List<double> timestamps =
        (timestampsY + timestampsX + timestampsZ).toSet().toList();
    timestamps.sort();
    for (double time in timestamps) {
      point = read(time);
      trace.add(Tuple4<double, double, double, double>(
          time, point.item1, point.item2, point.item3));
    }
    return trace;
  }
}
