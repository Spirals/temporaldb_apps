library temporaldb;

import 'dart:io';
import 'dart:convert';

import 'package:temporaldb/models/temporal_model.dart';
import 'package:temporaldb/temporal_db_1d_factory.dart';
import 'package:tuple/tuple.dart';

/// This class models GPS traces.
///
/// It uses two [TemporalModel] instances, one for latitude, one for longitude.
/// By default, FLI is used with an error of 0.001.
///
/// Note that points have to be added in chronological order.
class GPSDB {
  late final TemporalModel latM;
  late final TemporalModel lngM;

  double startingTime = -1;
  double currentTime = -1;
  int nbPoints = 0;

  // List<double> timestamps=[];

  /// Creates a GPS database.
  ///
  /// By default, it uses FLI with an error of 0.001.
  /// To see more about GPS Lat/Lng Precision:
  /// https://gis.stackexchange.com/questions/8650/measuring-accuracy-of-latitude-and-longitude
  /// https://support.oxts.com/hc/en-us/articles/115002885125-Level-of-Resolution-of-Longitude-and-Latitude-Measurements
  GPSDB([int type = 0, Map<String, String>? map = const {'error': '0.001'}]) {
    latM = Factory(type, map);
    lngM = Factory(type, map);
    return;
  }

  /// Adds a GPS point ([lat], [lng]) at timestamp [t].
  void add(double t, double lat, double lng) {
    if (startingTime == -1) {
      startingTime = t;
    }
    latM.add(t, lat);
    lngM.add(t, lng);
    nbPoints++;
    currentTime = t;
    // timestamps.add(t);
  }

  /// Returns the GPS position at timestamp [t].
  Tuple2<double, double> read(double t) {
    return Tuple2(latM.read(t), lngM.read(t));
  }

  /// Returns the count of double/int values used by the model.
  int getSize() {
    return latM.getSize() + lngM.getSize() + 3; //+timestamps.length;
  }

  /// Returns the trace associated to the input [timestamps].
  List<Tuple3<double, double, double>> getTrace(List<double> timestamps) {
    List<Tuple3<double, double, double>> trace = [];
    Tuple2<double, double> point;
    for (double time in timestamps) {
      point = read(time);
      trace.add(Tuple3<double, double, double>(time, point.item1, point.item2));
    }
    return trace;
  }

  /// Returns the trace associated to the timestamps of the models.
  List<Tuple3<double, double, double>> getTraceFromScratch() {
    if (nbPoints == 0) {
      return [];
    }
    Tuple2<double, double> point;
    if (nbPoints == 1) {
      point = read(startingTime);
      return [
        Tuple3<double, double, double>(startingTime, point.item1, point.item2)
      ];
    }
    List<Tuple3<double, double, double>> trace = [];

    List<double> timestampsLat = latM.getTimestamps();
    List<double> timestampsLng = lngM.getTimestamps();
    List<double> timestamps = (timestampsLng + timestampsLat).toSet().toList();
    timestamps.sort();
    for (double time in timestamps) {
      point = read(time);
      trace.add(Tuple3<double, double, double>(time, point.item1, point.item2));
    }
    return trace;
  }

  /*
  /// Exports the database in a file, compressed using GZIP.
  void writeDB(String add, String fileName) {
    List<Tuple2<double, List<double>>> mLat = latM.getModelsAndTimestamps();
    List<Tuple2<double, List<double>>> mLng = lngM.getModelsAndTimestamps();
    String content =
        "{$startingTime;$currentTime;$nbPoints;${mLat.toString()};${mLng.toString()}";
    List<int> stringBytes = utf8.encode(content);
    List<int> compressedBytes = gzip.encode(stringBytes);
    writeResultAsBytes(add + fileName, compressedBytes);
  }*/

  /*
  void loadDB(String add, String fileName) {
    final file = File(add+fileName);
    String compressedString = file.readAsStringSync();
    List<int> stringBytes = utf8.encode(compressedString);
    List<int> uncompressedBytes = gzip.decode(stringBytes);
    String uncompressedString = utf8.decode(uncompressedBytes);
    List<String> split = (uncompressedString.substring(0,uncompressedString.length-1)).split(";");
    startingTime = double.parse(split[0]);
    currentTime = double.parse(split[1]);
    nbPoints = int.parse(split[2]);
  }
*/
}
