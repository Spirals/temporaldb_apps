library temporaldb;

import 'package:flutter/material.dart';
import 'package:tuple/tuple.dart';
import 'dart:collection';

import 'temporal_model.dart';

/// Sliding Windows And Bottom-up (SWAB): uses a sliding window to compute a
/// local bottom-up approach.
///
/// Keogh, E., Chu, S., Hart, D., & Pazzani, M. (2004). Segmenting time series:
/// A survey and novel approach. In Data mining in time series databases (pp.
/// 1-21).
///
/// *********************************************
/// We only consider interpolation, no regression
/// *********************************************
///
///
/// Bottom-up and Top-down approaches are also included as static methods.
///
/// They are not considered as model per see since they are not online
/// approaches, and do not have a "add" method. A static [readFromModel]
/// function is provided to read from such a model.
///
/// In this class, the lists of points is represented by a list of Tuples
/// (t, v); the models are represented by a list of Tuples3 (t,A,B) such that at
/// t, the model is AX+B.

/// The default value for the error is 0.01.
class SWAB implements TemporalModel {
  double firstStartingTime = -1;

  /// Models
  /// Each model has a starting time and a polynomial regression; a model
  /// (t, A, B) estimates v_ at time t_ by v_ = A * t_ + B.
  List<Tuple3<double, double, double>> listOldModels = [];

  /// We also model the points in the sliding window.
  ListQueue<Tuple3<double, double, double>> listCurrentModels =
      ListQueue<Tuple3<double, double, double>>();

  /// The maximum tolerated error for the whole model.
  double maxError = 0.01;

  /// The minimum and maximum sizes of the sliding window.
  int maxSizeSW = 100;

  /// The sliding windows, called w in the paper.
  ListQueue<Tuple2<double, double>> slidingWindow =
      ListQueue<Tuple2<double, double>>();

  /// The starting time of the current model.
  double currentStartingTime = -1;

  /// SWAB combines a sliding window approach with the bottom-up approaches.
  /// We store the incoming points in a slidingWindow; if there are too many
  /// points, we use the bottom-up approach to model the sliding window and add
  /// the first model to the list of old models.
  /// We prune the sliding window of the corresponding points.
  @override
  void add(double t, double v) {
    /// We add the point to the sliding window (SW).
    slidingWindow.add(Tuple2(t, v));

    /// First insertion, we initialize the variables.
    if (firstStartingTime == -1) {
      firstStartingTime = t;
      currentStartingTime = t;
      listCurrentModels.add(Tuple3<double, double, double>(t, 0, v));
      // listCurrentModels = ListQueue<Tuple3<double,double,double>>.from(bottomUp(slidingWindow.toList(), maxError));
      return;
    }

    /// If the list of current models is empty, it means that all the points
    /// were removed the last time a model was added to listOldModels: the SW
    /// has been emptied too, we only need to add a basic polynomial (t, 0, v)
    /// as current model.
    if (listCurrentModels.isEmpty) {
      currentStartingTime = t;
      listCurrentModels.add(Tuple3<double, double, double>(t, 0, v));
      // listCurrentModels = ListQueue.from(bottomUp(slidingWindow.toList(), maxError));
      return;
    }

    /// If the point fits in the last model and the sliding windows is not full,
    /// we do not need to do anything.
    if (slidingWindow.length <= maxSizeSW &&
        (_getValue(t, listCurrentModels.last.item2,
                        listCurrentModels.last.item3) -
                    v)
                .abs() <=
            maxError) {
      return;
    }

    /// Otherwise, we need to model again the points in the sliding window. If
    /// the sliding window is full, we extract the first model and prune the
    /// corresponding points of the sliding window.
    // listCurrentModels = ListQueue.from(bottomUp(slidingWindow.toList(), maxError));
    listCurrentModels = bottomUpQueue(slidingWindow.toList(), maxError);
    if (slidingWindow.length > maxSizeSW) {
      listOldModels.add(listCurrentModels.first);
      listCurrentModels.removeFirst();
      // listCurrentModels.removeAt(0); /// Only one remove first, maybe does not worth transforming into a ListQueue?
      if (listCurrentModels.isEmpty) {
/*        debugPrint('Clearing the SW, $currentStartingTime');
        debugPrint('Last forgotten time:, ${slidingWindow.last.item1}');
        debugPrint('Associated time:, ${_getValue(slidingWindow.last.item1, listOldModels.last.item2, listOldModels.last.item3)}');
        debugPrint('inserted point: $t $v');*/
        slidingWindow.clear();
        return;
      }
      currentStartingTime = listCurrentModels.first.item1;
      while (slidingWindow.first.item1 < currentStartingTime) {
        slidingWindow.removeFirst();
      }
    }

    return;
  }

  @override
  List<List<double>> getModels() {
    List<List<double>> models = [];
    for (var tuple3 in listOldModels) {
      models.add([tuple3.item2, tuple3.item3]);
    }
    for (var tuple3 in listCurrentModels) {
      models.add([tuple3.item2, tuple3.item3]);
    }
    return models;
  }

  @override
  int getNbModels() {
    return (listOldModels.length + listCurrentModels.length);
  }

  @override
  int getSize() {
    return (4 +
        (listOldModels.length + listCurrentModels.length) * 3 +
        slidingWindow.length * 2);
  }

  @override
  List<double> getTimestamps() {
    List<double> timestamps = [];
    for (var tuple3 in listOldModels) {
      timestamps.add(tuple3.item1);
    }
    for (var tuple3 in listCurrentModels) {
      timestamps.add(tuple3.item1);
    }
    return timestamps;
  }

  @override
  List<Tuple2<double, List<double>>> getModelsAndTimestamps() {
    List<Tuple2<double, List<double>>> modelsAndTimestamps = [];
    for (var tuple3 in listOldModels) {
      modelsAndTimestamps
          .add(Tuple2(tuple3.item1, [tuple3.item2, tuple3.item3]));
    }
    for (var tuple3 in listCurrentModels) {
      modelsAndTimestamps
          .add(Tuple2(tuple3.item1, [tuple3.item2, tuple3.item3]));
    }
    return modelsAndTimestamps;
  }

  @override
  String getType() {
    return "SWAB";
  }

  @override
  double read(double t) {
    /// If the reading time is before the time of the first model, we throw.
    if (firstStartingTime == -1 || t < firstStartingTime) {
      throw ErrorDescription("Error the time is unavailable");
    }
    Tuple3<double, double, double>? model;
    if (currentStartingTime <= t) {
      for (var currentModel in listCurrentModels) {
        if (currentModel.item1 > t) {
          // j = i-1;
          break;
        }
        model = currentModel;
      }
      if (model != null) {
        return _getValue(t, model.item2, model.item3);
      }
      // for(var tuple3 in listCurrentModels.toList().reversed) {
      //   if (tuple3.item1 <= t) {
      //     return _getValue(t, tuple3.item2, tuple3.item3);
      //   }
      // }
    }
    for (var currentModel in listOldModels) {
      if (currentModel.item1 > t) {
        // j = i-1;
        break;
      }
      model = currentModel;
    }
    if (model != null) {
      return _getValue(t, model.item2, model.item3);
    }
    // for(var tuple3 in listOldModels.reversed) {
    // if (tuple3.item1 <= t) {
    //   return _getValue(t, tuple3.item2, tuple3.item3);
    // }
    // }
/*    /// This should be unreachable thanks to the first if condition.
    troubleShooting();
    debugPrint('Read time:');
    debugPrint(t);*/
    throw ErrorDescription("Error the time is unavailable (!)");
  }

/*  void troubleShooting() {
    debugPrint('First starting time: $firstStartingTime');
    debugPrint('Current starting time: $currentStartingTime');
    debugPrint('Old models:');
    debugPrint(listOldModels.toString());
    debugPrint('Current models:');
    debugPrint(listCurrentModels.toString());
    debugPrint('Sliding window:');
    debugPrint(slidingWindow.toString());
  }*/

  /// Returns the estimated value for t by the polynomial Ax+B, centered in T,
  /// i.e. A(t-T)+B.
  static double _getValue(double t, double A, double B) {
    return (A * t + B);
  }

  @override
  void setParameters(Map<String, String> map) {
    map.forEach((key, value) {
      switch (key) {
        case "error":
          {
            maxError = double.parse(value);
          }
          break;
        case "maxSizeSW":
          {
            maxSizeSW = int.parse(value);
          }
          break;
      }
    });
  }

  @override
  String toString() {
    String s = "SWAB model:\n";
    s += "[\n";
    for (var tuple3 in listOldModels) {
      s += "\t" +
          tuple3.item1.toString() +
          ": " +
          tuple3.item2.toString() +
          " " +
          tuple3.item3.toString() +
          "\n";
    }
    for (var tuple3 in listCurrentModels) {
      s += "\t" +
          tuple3.item1.toString() +
          ": " +
          tuple3.item2.toString() +
          " " +
          tuple3.item3.toString() +
          "\n";
    }
    s += "]";
    return s;
  }

  /// As BottomUp approach, returns a list of (timestamp, A, B).
  ///
  /// The bottom-up approach creates the finest possible approximation of the
  /// time series (n/2 segments), and then iteratively merge each pair of
  /// adjacent at lowest cost pair until a stopping criteria is met.
  ///
  /// The name of the variables are inspired from the paper of SWAB: a
  /// [ListQueue] is used as it is the data structure used in SWAB.
  static ListQueue<Tuple3<double, double, double>> bottomUpQueue(
      List<Tuple2<double, double>> T, double maxError) {
    /// The original algorithm is a pseudo algorithm; they use without
    /// distinction the model and the associated points. `segPoints` is used to
    /// manipulate the list of points.
    ListQueue<Tuple3<double, double, double>> segTS = ListQueue();

    /// Case where there is no enough elements (<2) for bottom up approach.
    /// If the list is empty, an empty list is returned; if there is only one
    /// element, we return a list with a simple polynomial.
    if (T.isEmpty) {
      return segTS;
    }
    if (T.length == 1) {
      segTS.add(Tuple3<double, double, double>(T[0].item1, 0, T[0].item2));
      return segTS;
    }

    /// `segTS` will be the associated models.
    List<List<Tuple2<double, double>>> segPoints = [];
    List<double> mergeCost = [];
    double minMergeCost = double.maxFinite;
    int indexMinMergeCost = -1;
    double cost;

    /// Create initial fine approximation.
    /// In the original paper, the step is 2, i.e. we have i=i+2;
    /// unfortunately, it creates some discontinuities between the different
    /// piecewise models as it may skip interesting joints. In particular, it
    /// may create an issue with the last point (odd size T).
    for (int i = 0; i < T.length - 1; i = i + 1) {
      segPoints += [T.sublist(i, i + 2)];
    }

    /// Find cost of merging each pair of segments.
    for (int i = 0; i < segPoints.length - 1; i++) {
      cost = calculateError(segPoints[i] + segPoints[i + 1]);
      mergeCost.add(cost);
      if (cost < minMergeCost) {
        minMergeCost = cost;
        indexMinMergeCost = i;
      }
    }

    while (minMergeCost < maxError) {
      /// While not finished.
      if (indexMinMergeCost == -1 ||
          segPoints.length == 1 ||
          segPoints[indexMinMergeCost].length == T.length) {
        break;
      }
      segPoints[indexMinMergeCost] =
          segPoints[indexMinMergeCost] + segPoints[indexMinMergeCost + 1];
      segPoints.removeAt(indexMinMergeCost + 1);
      if (indexMinMergeCost != mergeCost.length - 1) {
        mergeCost.removeAt(indexMinMergeCost + 1);
      } else {
        mergeCost.removeLast();
      }

      /// In the case where `indexMinMergeCost` is equal to segPoints.length-2,
      /// there is no next element anymore since we deleted the index
      /// indexMinMergeCost+1 for setPoints and mergeCost:
      /// setPoints.length-2 has become setPoints.length-1 since the deletion.
      if (indexMinMergeCost != segPoints.length - 1) {
        mergeCost[indexMinMergeCost] = calculateError(
            segPoints[indexMinMergeCost] + segPoints[indexMinMergeCost + 1]);
      }
      if (indexMinMergeCost != 0) {
        mergeCost[indexMinMergeCost - 1] = calculateError(
            segPoints[indexMinMergeCost - 1] + segPoints[indexMinMergeCost]);
      }

      /// Update `minMergeCost` to find the "cheapest" pair to merge.
      if (mergeCost.isEmpty) {
        break;
      }
      minMergeCost = mergeCost[0];
      indexMinMergeCost = 0;
      for (int i = 1; i < mergeCost.length; i++) {
        if (mergeCost[i] < minMergeCost) {
          minMergeCost = mergeCost[i];
          indexMinMergeCost = i;
        }
      }
    }

    /// Transforming the segments in models.
    for (List<Tuple2<double, double>> pointsSeg in segPoints) {
      segTS.add(getModelFromList(pointsSeg));
    }
    return segTS;
  }

  /// TODO n/2 or n-1 first segments?
  /// Returns a list of (timestamp, A, B).
  ///
  /// The bottom-up approach creates the finest possible approximation of the
  /// time series (n/2 segments), and then iteratively merge each pair of
  /// adjacent at the lowest cost pair until a stopping criteria is met.
  ///
  /// The name of the variables are inspired from the paper of SWAB.
  static List<Tuple3<double, double, double>> bottomUp(
      List<Tuple2<double, double>> T, double maxError) {
    /// Case where there is no enough elements (<2) for bottom up approach.
    /// If the list is empty, an empty list is return; if there is only one
    /// element, we return a list with a simple polynomial.
    if (T.isEmpty) {
      return [];
    }
    if (T.length == 1) {
      return [Tuple3<double, double, double>(T[0].item1, 0, T[0].item2)];
    }

    /// The original algorithm is a pseudo algorithm.
    /// They use without distinction the model and the associated points:
    /// `segPoints` is used to manipulate list of points, while `segTS` will be
    /// the associated models.
    List<Tuple3<double, double, double>> segTS = [];
    List<List<Tuple2<double, double>>> segPoints = [];
    List<double> mergeCost = [];
    double minMergeCost = double.maxFinite;
    int indexMinMergeCost = -1;
    double cost;

    /// Create initial fine approximation.
    /// In the original paper, the step is 2, i.e. we have i=i+2;
    /// unfortunately, it creates some discontinuities between the different
    /// piecewise models as it may skip interesting joints. In particular, it
    /// may create an issue with the last point (odd size T).
    for (int i = 0; i < T.length - 1; i = i + 1) {
      segPoints += [T.sublist(i, i + 2)];
    }

    /// Find cost of merging each pair of segments.
    for (int i = 0; i < segPoints.length - 1; i++) {
      cost = calculateError(segPoints[i] + segPoints[i + 1]);
      mergeCost.add(cost);
      if (cost < minMergeCost) {
        minMergeCost = cost;
        indexMinMergeCost = i;
      }
    }
/*    debugPrint('init state:');
    debugPrint(segPoints.length);
    debugPrint(segPoints.toString());
    debugPrint(mergeCost.length);
    debugPrint(mergeCost.toString());*/
    // int indexPrint=0;

    while (minMergeCost < maxError) {
      /// While not finished.
      if (indexMinMergeCost == -1 ||
          segPoints.length == 1 ||
          segPoints[indexMinMergeCost].length == T.length) {
        break;
      }
      /*
      debugPrint('log:');
      debugPrint(indexMinMergeCost);
      debugPrint(segPoints.length);
      debugPrint(segPoints[indexMinMergeCost].length);
      debugPrint(T.length); */
/*      if(indexPrint<=5000) {
        debugPrint('iter $indexPrint state:');
        debugPrint(segPoints.length);
        debugPrint(segPoints.toString());
        debugPrint(mergeCost.length);
        debugPrint(mergeCost.toString());
        indexPrint++;
      }
      debugPrint(minMergeCost);
      debugPrint(indexMinMergeCost);*/
      segPoints[indexMinMergeCost] =
          segPoints[indexMinMergeCost] + segPoints[indexMinMergeCost + 1];
      segPoints.removeAt(indexMinMergeCost + 1);
      if (indexMinMergeCost != mergeCost.length - 1) {
        mergeCost.removeAt(indexMinMergeCost + 1);
      } else {
        mergeCost.removeLast();
      }

      /// In the case where `indexMinMergeCost` is equal to segPoints.length-2,
      /// there is no next element anymore since we deleted the index
      /// indexMinMergeCost+1 for setPoints and mergeCost:
      /// setPoints.length-2 has become setPoints.length-1 since the deletion.
      if (indexMinMergeCost != segPoints.length - 1) {
        mergeCost[indexMinMergeCost] = calculateError(
            segPoints[indexMinMergeCost] + segPoints[indexMinMergeCost + 1]);
      }
      if (indexMinMergeCost != 0) {
        mergeCost[indexMinMergeCost - 1] = calculateError(
            segPoints[indexMinMergeCost - 1] + segPoints[indexMinMergeCost]);
      }

      /// Update minMergeCost to find the "cheapest" pair to merge.
      if (mergeCost.isEmpty) {
        break;
      }
      minMergeCost = mergeCost[0];
      indexMinMergeCost = 0;
      for (int i = 1; i < mergeCost.length; i++) {
        if (mergeCost[i] < minMergeCost) {
          minMergeCost = mergeCost[i];
          indexMinMergeCost = i;
        }
      }
    }

    /// Transforming the segments in models.
    for (List<Tuple2<double, double>> pointsSeg in segPoints) {
      segTS += [getModelFromList(pointsSeg)];
    }
/*    debugPrint('final state:');
    debugPrint(segPoints.length);
    debugPrint(segPoints.toString());
    debugPrint(mergeCost.length);
    debugPrint(mergeCost.toString());*/
    return segTS;
  }

  /// As TopDown approach, returns a list of (timestamp, A, B).
  ///
  /// The top-down approach considers all the possible splits of the lists.
  /// Every time, the new error is computed on each part, the list is cut at the
  /// best index; if the cuts have errors higher than a given threshold, they
  /// are recursively split.
  ///
  /// The name of the variables are inspired from the paper of SWAB.
  static List<Tuple3<double, double, double>> topDown(
      List<Tuple2<double, double>> T, double maxError) {
    List<Tuple3<double, double, double>> segTS = [];

    /// The index of the best cut.
    int breakpoint = -1;

    /// Best error.
    double bestSoFar = calculateError(T);
    double newError;
    for (int i = 1; i < T.length - 1; i++) {
      newError = calculateError(T.sublist(0, i + 1)) +
          calculateError(T.sublist(i, T.length));
      if (newError < bestSoFar) {
        breakpoint = i;
        bestSoFar = newError;
      }
    }
    if (breakpoint == -1) {
      return [getModelFromList(T)];
    }

    /// Recursively split the left segment if necessary.
    if (calculateError(T.sublist(0, breakpoint + 1)) > maxError) {
      segTS = topDown(T.sublist(0, breakpoint + 1), maxError);
    } else {
      segTS = [getModelFromList(T.sublist(0, breakpoint + 1))];
    }

    /// Recursively split the right segment if necessary.
    if (calculateError(T.sublist(breakpoint, T.length)) > maxError) {
      segTS += topDown(T.sublist(breakpoint, T.length), maxError);
    } else {
      segTS += [getModelFromList(T.sublist(breakpoint, T.length))];
    }
    return segTS;
  }

  /// Returns the error made by an interpolation between the first and last
  /// points on the other points.
  static double calculateError(List<Tuple2<double, double>> ts) {
    double A =
        (ts.last.item2 - ts.first.item2) / (ts.last.item1 - ts.first.item1);
    double B = ts.first.item2 - A * ts.first.item1;
    double error = 0;
    for (var tuple2 in ts) {
      error += (tuple2.item2 - (A * tuple2.item1 + B)).abs();
    }
    return error;
  }

  /// Returns the model (t, A, B) such that from t, to obtain the value v_ at
  /// time t_, we have: v_ = A * t_ + B.
  static Tuple3<double, double, double> getModelFromList(List<Tuple2> points) {
    double A = (points.last.item2 - points.first.item2) /
        (points.last.item1 - points.first.item1);
    double B = points.first.item2 - A * points.first.item1;
    return Tuple3(points.first.item1, A, B);
  }

  /// Estimates the value of [t] by the model.
  static double readFromModel(
      double t, List<Tuple3<double, double, double>> model) {
    /// If the reading time is before the time of the first model, we throw an error
    if (t < model.first.item1) {
      throw ErrorDescription("Error the time is unavailable");
    }

    /// We go through the stored models from the closer to the latest.
    for (var tuple3 in model.reversed) {
      double startingTime = tuple3.item1;

      /// If the reading time is within the limits of the currently considered
      /// model, we predict the value from it.
      if (startingTime <= t) {
        return (tuple3.item2 * t + tuple3.item3);
      }
    }

    /// This should be unreachable thanks to the first if condition.
    throw ErrorDescription("Error the time is unavailable (!)");
  }
}
