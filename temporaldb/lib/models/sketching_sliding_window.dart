library temporaldb;

import 'package:scidart/numdart.dart';
import 'package:temporaldb/models/sketching.dart';
import 'package:tuple/tuple.dart';
import 'temporal_model.dart';
import 'dart:collection';

class SketchDBSW extends SketchDB implements TemporalModel {
  /// Variant of [Greycat], extending [SketchDB] by keeping the list of the last
  /// inserted points, while also storing their error with respect to the current
  /// model.
  ///
  /// We use those points to find a better model. The lists are cleared if enough
  /// points are fitting well enough, i.e. their error is lower than errorSmall:
  /// (errorSmall <= error/10)
  ///
  /// Old params:
  ///
  /// Timestamp (non normalized) of the first inserted point
  /// It is used to know if a read is out of bounds
  /// double firstStartingTime=-1;
  ///
  /// Values used for normalisation
  /// double timeValueNormalization = pow(2,22).toDouble();
  /// double yValueNormalization=0;
  ///
  /// Default value for all models
  /// int maxDegree = 14; /// maximum value for the degree of the polynomial regression (PolyFit)
  /// double error=0.01; /// Tolerated error
  ///
  /// Models
  /// Each model has a starting time and a polynomial regression
  /// List<Tuple2<double, PolyFit>> listOldModels = [];
  ///
  /// PolyFit currentModel= PolyFit(Array([0]),Array([0]),0);
  /// double currentStartingTime=0; /// Starting time of the currentModel
  /// double currentTime = 0; /// Last addition => used to regenerate the points used for modeling
  ///
  ///
  /// Variables of SketchDB
  ///  int numberPointsStabilityModel = 100; /// Number of points for a model to be kept
  ///  int numberOfPoints = 0; /// Number of points fitting in the currentModel

  ListQueue<double> listT = ListQueue<double>();
  ListQueue<double> listV = ListQueue<double>();
  ListQueue<double> listError = ListQueue<double>();
  double errorSmall = 0.001;
  int sizeSlidingWindow = 100;
  int numberPointsSmallErrorBeforeResetWindow = 25;
  int numberSmallError = 0;
//  int numberToleratedOutliers = 0;

  SketchDBSW([double error_ = 0.01]) {
    error = error_;
  }

  @override
  void setParameters(Map<String, String> map) {
    map.forEach((key, value) {
      switch (key) {
        case "timeValueNormalization":
          {
            timeValueNormalization = double.parse(value);
          }
          break;
        case "yValueNormalization":
          {
            yValueNormalization = double.parse(value);
          }
          break;
        case "error":
          {
            error = double.parse(value);
          }
          break;
        case "maxDegree":
          {
            maxDegree = int.parse(value);
          }
          break;
        case "minNbPointsKeepModel":
          {
            numberPointsStabilityModel = int.parse(value);
          }
          break;
      }
    });
  }

  @override
  void add(double tRaw, double vRaw) {
    /// In all cases, the currentTime will be tRaw.
    currentTime = tRaw;

    double vNorm = vRaw;

    /// We normalize the value.
    if (yValueNormalization != 0) {
      vNorm = vRaw / yValueNormalization;
    }

    /// Set first point.
    if (firstStartingTime == -1) {
      setNewTimeAndModel(tRaw, vNorm);
      firstStartingTime = tRaw;

      /// We do not add the point in the lists as it perfectly fits.
      return;
    }

    /// We normalize the time.
    double tNorm = (tRaw - currentStartingTime) / timeValueNormalization;

    double errorT = (currentModel.predict(tNorm) - vNorm).abs();

    /// If the point fits, we cache it in the lists.
    /// We clear the lists when enough points with small enough error are added
    /// consecutively.
    if (errorT < error) {
      numberOfPoints++;

      /// The error is small enough, we increase our counter.
      if (errorT < errorSmall) {
        /// If the lists are empty, points are fitting well, nothing to do.
        if (listT.isEmpty) {
          return;
        }

        /// If the counter is reaching a threshold, we clear the lists.
        if (numberSmallError == numberPointsSmallErrorBeforeResetWindow - 1) {
          _clearLists();
          return;
        }

        /// Else, we increase the number counter.
        numberSmallError++;
        return;
      } else {
        /// We set the counter of small errors to zero.
        numberSmallError = 0;
      }

      /// We add the new points and check the size of the lists.
      _addPointVerifList(tNorm, vNorm, errorT);

      /// Since the current model is still fitting, we can stop here.
      return;
    }

    ///
    /// Here, the model does not fit the new value.
    /// If the model is stable, i.e. it predicts enough points, we keep it; in
    /// that case, we model on the list and the new point.
    /// If not, we regenerate points to represent the forgotten points and we
    /// model on them, the cached points and the new point.
    ///

    if (keepModel()) {
      listOldModels.add(Tuple2(currentStartingTime, currentModel));

      List<double> listT_ = listT.toList();
      List<double> listV_ = listV.toList();
      listT_.add(tNorm);
      listV_.add(vNorm);
      numberOfPoints++;

      //TODO update to have the list of errors with it ?
      PolyFit? p = fitPolynomialWithList(Array(listT_), Array(listV_), 0);
      if (p == null) {
        setNewTimeAndModel(tRaw, vNorm);

        /// We do not add the current point as it perfectly fits.
      } else {
        currentModel = p;
        currentStartingTime = listT_.first + currentStartingTime;
        numberOfPoints = listT_.length;
        _clearLists();
        _updateValueListNewModel(listT_, listV_);
      }
      return;
    }

    /// Case where the value doesn'tNorm fit in the model : we need a new one.

    ///
    /// If we do not keep the model:
    /// 1) we try to find a new model for all the points, even those forgotten
    /// -> regenerate [0,listT.first] and try to fit on [0,tNorm];
    /// 2) if it fails, try to fit on [listT.first,tNorm] (i.e. on the lists +
    /// tNorm);
    /// 3) if the obtained sse is lower than the previous one, we keep the model
    /// and only model on tNorm.
    ///

    int degree = currentModel.degree;

    /// Not +1: we try to fit another polynomial with the same degree first.

    PolyFit? temporaryModel;

    /// If the lists are empty, it is similar to Greycat.
    if (listT.isEmpty) {
      temporaryModel = fitPolynomial(tNorm, vNorm, degree);
      if (temporaryModel != null) {
        currentModel = temporaryModel;
        numberOfPoints++;
      } else {
        listOldModels.add(Tuple2(currentStartingTime, currentModel));
        setNewTimeAndModel(tRaw, vNorm);
      }
      return;
    } else {
      /// Otherwise, we try first to fit with generated points and the lists.
      List<double> listT_ = listT.toList();
      List<double> listV_ = listV.toList();
      listT_.add(tNorm);
      listV_.add(vNorm);
      double endGeneration;
      if (!listT.isEmpty) {
        endGeneration = listT.first;
      } else {
        endGeneration = currentTime;
      }
      var points = generatePoints(currentStartingTime, endGeneration,
          min(listT.length, numberOfPoints - listT.length));
      temporaryModel = fitPolynomialWithList(
          points.item1 + listT_, points.item2 + listV_, degree);
      if (temporaryModel == null) {
        /// if it does not work, we do it only with the lists.
        listT_ = _retrieveStartingTime(listT_, listT_.first);
        temporaryModel = fitPolynomialWithList(listT_, listV_, 0);
        if (temporaryModel == null) {
          /// We fail: the new point is too different, we only fit the new element.
          setNewTimeAndModel(tRaw, vNorm);
          return;
        } else {
          /// We succeed, we check that the new model is better than the former;
          /// we compare the sse.
          double sse = _getSSE();
          double errorNewModelOnV =
              temporaryModel.predict(tNorm - listT_.first);
          if (sse < temporaryModel.sse - errorNewModelOnV * errorNewModelOnV) {
            /// The former model was better for the points in the lists; we only
            /// fit the new point.
            setNewTimeAndModel(tRaw, vNorm);
            return;
          } else {
            /// The new model is good enough, so we keep it, save it and update
            /// the lists.
            currentModel = temporaryModel;
            currentStartingTime = listT_.first + currentStartingTime;
            numberOfPoints = listT_.length;
            _updateValueListNewModel(listT_, listV_);
            return;
          }
        }
      } else {
        /// The new model is fitting the new point; we save it and update the
        /// lists.
        currentModel = temporaryModel;
        _updateValueListNewModel(listT_, listV_);
        return;
      }
    }

    /// TODO REMOVE
/*
    double endGeneration;
    if (! listT.isEmpty) {
      endGeneration = listT.first;
    }
    else {
      endGeneration = currentTime;
    }
    var points = generatePoints(currentStartingTime, endGeneration, degree); //, degree+1, currentModel);



    List<double> listT_ = listT.toList();
    List<double> listV_ = listV.toList();
    listT_.add(tNorm);
    listV_.add(vNorm);
    numberOfPoints++;


    /// We model on the regenerated points plus the ones in the lists (including tNorm)
    temporaryModel = fitPolynomWithList(points.item1 + listT_, points.item2 + listV_, degree);

    if(temporaryModel == null) {
      /// We cannot fit all points, we save the old model
      listOldModels.add(Tuple2(currentStartingTime, currentModel));
      /// We try to fit all the points in listT and listV
      /// We new starting time should be the time of the first stored element
      listT_ = _retrieveStartingTime(listT_, listT_.first);
      temporaryModel = fitPolynomWithList(listT_, listV_,0);
      if(temporaryModel==null) {
        /// We fail: the new point it too different, we only fit the new element
        setNewTimeAndModel(tRaw,vNorm);
        return;
      }
      else {
        /// We succeed, we check that the new model is better than the former
        /// we compare the sse
        double sse = _getSSE();
        double errorNewModelOnV = temporaryModel.predict(tNorm-listT_.first);
        if (sse < temporaryModel.sse - errorNewModelOnV*errorNewModelOnV) {
          /// The former model was better for the points in the lists
          /// We only fit the new point
          setNewTimeAndModel(tRaw,vNorm);
          return;
        }
        else {
          /// the new model is good enough we keep it
          /// We save the model and update the lists
          currentModel = temporaryModel;
          currentStartingTime=listT_.first+currentStartingTime;
          numberOfPoints=listT_.length;
          _updateValueListNewModel(listT_,listV_);
          return;
        }
      }
    }
    else {
      /// The new model is fitting the new point
      /// We save the model and update the lists
      currentModel = temporaryModel;
      _updateValueListNewModel(listT_,listV_);
      return;
    }
*/
  }

  PolyFit? fitPolynomialWithList(List<double> listX, List<double> listY,
      [int startingDegree = 0]) {
    Array arrayX = Array(listX);
    Array arrayY = Array(listY);
    int degree = startingDegree;
    PolyFit p;
    while (degree <= maxDegree) {
      try {
        p = PolyFit(arrayX, arrayY, degree);
        if (arePredictionsCorrect(p, listX, listY)) {
          return p;
        }
        degree++;
      } on FormatException {
        return null;
      }
    }
    return null;
  }

  void _updateValueListNewModel(List<double> listT_, List<double> listV_) {
    double x, y, e;
    _clearLists();
    for (var i = 0; i < listT_.length; i++) {
      x = listT_[i];
      y = listV_[i];
      e = (currentModel.predict(x) - y).abs();
      _addPointList(x, y, e);

      /// No verification needed since we cleared the lists.
      _updateSmallError(e);
    }
    while (numberOfPoints > sizeSlidingWindow) {
      listT.removeFirst();
      listV.removeFirst();
      listError.removeFirst();
    }
    return;
  }

  void _updateSmallError(double e) {
    if (e < errorSmall) {
      /// Empty listT case
      /// Only one element which is the one associated with (t, v, e).
      if (listT.length == 1) {
        _clearLists();
        return;
      }
      numberSmallError++;
      if (numberSmallError >= numberPointsSmallErrorBeforeResetWindow) {
        _clearLists();
      }
    } else {
      numberSmallError = 0;
    }
  }

  List<double> _retrieveStartingTime(
      List<double> originalList, double startingTime) {
    List<double> newList = [];
    for (var element in originalList) {
      newList.add(element - startingTime);
    }
    return newList;
  }

  double _getSSE() {
    double sse = 0;
    for (var element in listError) {
      sse = sse + element * element;
    }
    return sse;
  }

  void _clearLists() {
    listT.clear();
    listV.clear();
    listError.clear();
    numberSmallError = 0;
  }

  void _addPointList(double t, double v, double error) {
    listT.add(t);
    listV.add(v);
    listError.add(e);
  }

  void _addPointVerifList(double t, double v, double error) {
    _addPointList(t, v, error);

    /// If the lists are too large, we remove the older point.
    if (listT.length > sizeSlidingWindow) {
      listT.removeFirst();
      listV.removeFirst();
      listError.removeFirst();
    }
  }

  /// Returns the count of 64-bits values in the total model.
  @override
  int getSize() {
    int size = super.getSize();
    size = size + 3 * listT.length;

    /// listT listV and listError
    size = size + 4;

    /// The four variables.
    return size;
  }

  @override
  String getType() {
    return "SketchingDBSW";
  }

  @override
  String toString() {
    String s = super.toString();
    s += 'Lists:\n';
    for (int i = 0; i < listT.length; i++) {
      s +=
          '\t${listT.elementAt(i)}, ${listV.elementAt(i)}, ${listError.elementAt(i)}\n';
    }
    return s;
  }
}
