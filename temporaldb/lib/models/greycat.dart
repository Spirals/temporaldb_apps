library temporaldb;

import 'package:flutter/material.dart';
import 'package:scidart/numdart.dart';
import 'package:tuple/tuple.dart';
import 'temporal_model.dart';

/// Greycat implementation.
///
/// Beyond Discrete Modeling: A Continuous and Efficient Model for IoT.
/// 2015 ACM/IEEE 18th International Conference on Model Driven Engineering
/// Languages and Systems (MODELS). Moawad, A., Hartmann, T., Fouquet, F., Nain,
/// G., Klein, J., & Le Traon, Y.
/// https://jacquesklein2302.github.io/papers/2015-models15_polynomial_Moawad_preprint.pdf
///
/// Greycat models the data with a piece-wise continuous model. It starts with a
/// model of degree 0 and increase the degree until either the model fits or a
/// maximum degree is reached. In the latter case, the model is kept and a new
/// model is initialized for the new data points.
///
/// When a data point is added, if the degree has to be increased, the points
/// used for the regression are generated with the current model: d+1 points
/// are generated, and to be accepted the new model must have an error lower
/// than 2^{-d-1} on those points, with d being the degree of the model.
class Greycat implements TemporalModel {
  double firstStartingTime = -1;

  /// The values used for normalisation.
  double timeValueNormalization = pow(2, 22).toDouble();
  double yValueNormalization = 0;

  /// The default value for all models.
  int maxDegree = 14;

  /// The maximum value for the degree of the polynomial regression (PolyFit).
  double error = 0.01;

  /// Tolerated error

  /// Models
  /// Each model has a starting time and a polynomial regression.
  List<Tuple2<double, PolyFit>> listOldModels = [];

  PolyFit currentModel = PolyFit(Array([0]), Array([0]), 0);
  double currentStartingTime = 0;

  /// The starting time of the currentModel.
  double currentTime = 0;

  /// Last addition => used to regenerate the points used for modeling.
  //  int numberOfPoints = 0; /// Number of points fitting in the currentModel => useless for Greycat

  /// The default value for the error is 0.01.
  Greycat([double error_ = 0.01]) {
    error = error_;
  }

  @override
  void setParameters(Map<String, String> map) {
    map.forEach((key, value) {
      switch (key) {
        case "timeValueNormalization":
          {
            timeValueNormalization = double.parse(value);
          }
          break;
        case "yValueNormalization":
          {
            yValueNormalization = double.parse(value);
          }
          break;
        case "error":
          {
            error = double.parse(value);
          }
          break;
        case "maxDegree":
          {
            maxDegree = int.parse(value);
          }
          break;
      }
    });
  }

  @override
  void add(double tRaw, double vRaw) {
    /// In all the cases, the currentTime will be tRaw.
    currentTime = tRaw;

    double vNorm = vRaw;
    if (yValueNormalization != 0) {
      vNorm = vRaw / yValueNormalization;
    }

    if (firstStartingTime == -1) {
      setNewTimeAndModel(tRaw, vNorm);
      firstStartingTime = tRaw;
      return;
    }

    var tNorm = (tRaw - currentStartingTime) / timeValueNormalization;

    if (isPredictionCorrect(currentModel, tNorm, vNorm)) {
      return;
    }
    // debugPrint('Point does not fit, trying new point...');
    int degree = currentModel.degree;
    // debugPrint('Current model of degree $degree:');
    // debugPrint(currentModel.coefficients().toString());
    // debugPrint('Error: $tNorm $vNorm ${currentModel.predict(tNorm)}');

    /// In the case where the value doesn't fit in the model.
    PolyFit? temporaryModel;
/*    var points = generatePoints(currentStartingTime, currentTime, degree+1, currentModel);
    points.item1.add(tNorm);
    points.item2.add(vNorm);
    temporaryModel = fitPolynom(Array(points.item1), Array(points.item2), degree); */
    temporaryModel = fitPolynomial(tNorm, vNorm, degree);
    if (temporaryModel != null) {
      currentModel = temporaryModel;
    } else {
      // debugPrint('NULL');
      listOldModels.add(Tuple2(currentStartingTime, currentModel));
      setNewTimeAndModel(tRaw, vNorm);
      return;
    }
  }

  void setNewTimeAndModel(double tRaw, double vNorm) {
//    currentTime=tRaw;
    currentStartingTime = tRaw;
//    numberOfPoints=1;
    currentModel = PolyFit(Array([0.0]), Array([vNorm]), 0);
  }

  PolyFit? fitPolynomial(double tNorm, double vNorm, [int startingDegree = 0]) {
    int degree = startingDegree + 1;
    PolyFit p;
    while (degree <= maxDegree) {
      try {
/*        if(listOldModels.isEmpty && degree == 3) {
          debugPrint('Degree $degree');
          debugPrint('Generating points...');
        }*/
        var generatedPoints = generatePoints(currentStartingTime, currentTime,
            degree); //, degree+1, currentModel);
/*        if(listOldModels.isEmpty && degree == 3) {
          debugPrint('Points generated!');
        }*/
        generatedPoints.item1.add(tNorm);
        generatedPoints.item2.add(vNorm);
        Array arrayX = Array(generatedPoints.item1);
        Array arrayY = Array(generatedPoints.item2);
/*        if(listOldModels.isEmpty && degree >= 3) {
          debugPrint('arrayX: ${arrayX.toList().toString()}');
          debugPrint('arrayY: ${arrayY.toList().toString()}');
          debugPrint('Trying to fit...');
        }*/
        p = PolyFit(arrayX, arrayY, degree);
/*        if(listOldModels.isEmpty && degree == 3) {
//        if(_printLog(p)) {
          debugPrint('Fit done!');
          debugPrint(p.coefficients().toList().toString());
          debugPrint(_getMAE(p.coefficients().toList(), [95.46726116652904, -41.22932499824357, 17.53111876212312, -81.78497030880662]));
        }*/
        if (arePredictionsCorrect(
            p, generatedPoints.item1, generatedPoints.item2)) {
          return p;
        }
/*        if(listOldModels.isEmpty && degree == 3) {
//        if(_printLog(p)) {
          debugPrint('Incorrect polynom, increasing degree...');
        }*/
        degree++;
      } on FormatException {
        return null;
      }
    }
    return null;
  }
  /*
  PolyFit? fitPolynom(List<double> listX, List<double> listY, [int startingDegree=0]) {
    Array arrayX = Array(listX);
    Array arrayY = Array(listY);
    int degree = startingDegree;
    PolyFit p;
    while (degree <= maxDegree) {
      try {
        p = PolyFit(arrayX, arrayY, degree);
        if (_arePredictionCorrect(p, listX, listY)) {
          return p;
        }
        degree++;
      }
      on FormatException {
        return null;
      }
    }
    return null;
  }
   */

  bool arePredictionsCorrect(
      PolyFit p, List<double> listX, List<double> listY) {
    /*
    if(_printLog(p)) {
      debugPrint('Printing lists again:');
      debugPrint(listX.toString());
      debugPrint(listY.toString());
      debugPrint('listX length: ${listX.length}');
      debugPrint('model: ${p.coefficients().toList().toString()}');
    }*/
    for (int i = 0; i < listX.length; i++) {
      /*
      if(listOldModels.isEmpty) {
        debugPrint('Printing i: $i');
      }*/
      if (!isPredictionCorrect(p, listX[i], listY[i])) {
        return false;
      } /*
      if(listOldModels.isEmpty) {
        debugPrint('Good so no returning');
      }*/
    } /*
    if(listOldModels.isEmpty) {
      debugPrint('returning true');
    }*/
    return true;
  }

  /* bool _printLog(PolyFit p) {
    return (_getMAE(p.coefficients().toList(), [95.46726116652904, -41.22932499824357, 17.53111876212312, -81.78497030880662]) <=0.001);
  }*/

  /* double _getMAE(List<double> coeff1, List<double> coeff2) {
    double mae = 0;
    if (coeff2.length != coeff1.length) {
      return -1;
    }
    for(var i=0; i<coeff1.length; i++) {
      mae = mae + (coeff1[i]-coeff2[i]).abs();
    }
    return mae;
  }
*/
  bool isPredictionCorrect(PolyFit poly, double t, double v) {
    double v_ = poly.predict(t);
    var error_ = (poly.predict(t) - v).abs();
    /*if(_printLog(poly)) {
      if (error_<=v_*error/(pow(2,poly.degree+1).toDouble())) {
        // debugPrint('GOOD VALUE: $t $v ${poly.predict(t)} $error_ ${(error/(pow(2,poly.degree+1).toDouble()))}');
      }
      else {
        debugPrint('WRONG VALUE: $t $v $v_ $error_ ${(error/(pow(2,poly.degree+1).toDouble()))}');
      }
    }*/
    if (v_ == 0) {
      return (error_ <= error / (pow(2, poly.degree + 1).toDouble()));
    }
    return (error_ <= v_ * error / (pow(2, poly.degree + 1).toDouble()));
  }

  Tuple2<List<double>, List<double>> generatePoints(
      double startingT, double endingT, degree) {
    //}, int nbPoints, PolyFit polynom) {
    if (degree == 0) {
      return Tuple2([0.0], [currentModel.predict(0.0)]);
    }
    List<double> listT = [];
    List<double> listV = [];
    double intervalLength = (endingT - startingT) / degree;
    if (timeValueNormalization != 0) {
      intervalLength = intervalLength / timeValueNormalization;
    }
    for (var i = 0; i < degree; i++) {
      double t = i * intervalLength;
      double v = currentModel.predict(t);
      listT.add(t);
      listV.add(v);
    }
    return Tuple2(listT, listV);
  }

  @override
  double read(double t) {
    /// If the reading time is before the time of the first model, we throw.
    if (t < firstStartingTime) {
      throw ErrorDescription("Error the time is unavailable");
    }

    /// If the reading time is within the limits of the current model, we
    /// predict from it.
    if (currentStartingTime <= t) {
      if (yValueNormalization == 0) {
        return currentModel
            .predict((t - currentStartingTime) / timeValueNormalization);
      } else {
        return (yValueNormalization *
            currentModel
                .predict((t - currentStartingTime) / timeValueNormalization));
      }
    }

    /// We go through the stored models from the closer to the latest.
    Tuple2<double, PolyFit>? model;
    for (var tuple2 in listOldModels) {
      if (tuple2.item1 > t) {
        break;
      }
      model = tuple2;
    }
    if (model != null) {
      double startingTime = model.item1;
      if (yValueNormalization == 0) {
        return model.item2.predict((t - startingTime) / timeValueNormalization);
      } else {
        return (yValueNormalization *
            model.item2.predict((t - startingTime) / timeValueNormalization));
      }
    }
    // for (var tuple2 in listOldModels.reversed) {
    //   double startingTime = tuple2.item1;
    //   /// If the reading time is within the limits of the currently considered model, we predict the value from it
    //   if (startingTime <= t) {
    //     if (yValueNormalization == 0) {
    //       return tuple2.item2.predict( (t - startingTime) / timeValueNormalization);
    //     }
    //     else {
    //       return (yValueNormalization * tuple2.item2.predict((t - startingTime) / timeValueNormalization));
    //     }
    //   }
    // }
    /// This should be unreachable thanks to the first `if` condition.
    throw ErrorDescription("Error the time is unavailable (!)");
  }

  ///
  /// Returns the count of 64-bits values in the total model.
  ///
  @override
  int getSize() {
    int size = 8;

    /// 8 variables double/int
    for (var m in listOldModels) {
      size = size + m.item2.degree + 1 + 1 + 3;

      /// Each model in the list has degree+1 coefficient, one timestamp, and 3
      /// variables in a PolyFit.
    }
    size = size + currentModel.degree + 1 + 3;
    return size;
  }

  @override
  int getNbModels() {
    return listOldModels.length + 1;
  }

  @override
  List<List<double>> getModels() {
    List<List<double>> models = [];
    for (var m in listOldModels) {
      models.add(m.item2.coefficients().toList());
    }
    models.add(currentModel.coefficients());
    return models;
  }

  @override
  List<double> getTimestamps() {
    List<double> listTimestamps = [];
    for (var m in listOldModels) {
      listTimestamps.add(m.item1);
    }
    listTimestamps.add(currentStartingTime);
    return listTimestamps;
  }

  @override
  List<Tuple2<double, List<double>>> getModelsAndTimestamps() {
    List<Tuple2<double, List<double>>> modelsAndTimestamps = [];
    for (var m in listOldModels) {
      modelsAndTimestamps.add(Tuple2(m.item1, m.item2.coefficients().toList()));
    }
    modelsAndTimestamps
        .add(Tuple2(currentStartingTime, currentModel.coefficients().toList()));
    return modelsAndTimestamps;
  }

  @override
  String getType() {
    return "Greycat";
  }

  @override
  String toString() {
    String s = 'Model ' + getType() + ':\n';
    s += '${listOldModels.length + 1} models\n';
    s += 'Starting at time $firstStartingTime to $currentTime.\n';
    s += 'Models:\n';
    for (var m in listOldModels) {
      s += '\t${m.item1} : ${m.item2.toString()}\n';
    }
    s += '\t$currentStartingTime : ${currentModel.coefficients().toString()}';
    return s;
  }
}
