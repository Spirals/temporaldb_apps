library temporaldb;

import 'package:scidart/numdart.dart';
import 'package:temporaldb/models/fast_linear_interpolation.dart';
import 'package:tuple/tuple.dart';
import 'dart:collection';

/// This class is an extension of [FastLinearInterpolation].
///
/// We store a fixed size sliding window consisting of the last added points.
/// When we detect a turning point, i.e. when we need to change the model, we go
/// back and try to see if there is another point which fits best than the last
/// added.
///
/// We now have two models: the current model (starting at currentStartingTime)
/// and the new model (ending at tRaw). Both these models have only one point on
/// common tTP (turning point); tTP is initially the last added point lastTimeRaw.
/// We change this point by going through the lists of points in reverse order.
///
/// We compute two values: the AE (absolute error) of the actual model and of
/// the new model while changing the tTP. We stop when the sum of the two AE
/// stops decreasing.
///
/// While it decreases, it means that we missed the turning point with our
/// optimistic approaches, the last points fitting better a new straight line.
/// The sum of the AE stops decreasing at the actual turning point.

class FLISlidingWindow extends FastLinearInterpolation {
  /// We store the last windowSize points.
  int windowSize = 100;
  ListQueue<Tuple2<double, double>> listPointsRaw =
      ListQueue<Tuple2<double, double>>();

  @override
  void add(double tRaw, double vRaw) {
    if (firstStartingTimeRaw == -1) {
      firstStartingTimeRaw = tRaw;
      _initModel(tRaw, vRaw);
      return;
    }

    double tNorm = tRaw - currentStartingTimeRaw;
    double vNorm = vRaw - currentStartingValueRaw;
    double A = vNorm / tNorm;

    /// If the points fits, we update the values and return.
    if (A <= maxPossibleA && A >= minPossibleA) {
      minPossibleA = max(minPossibleA, (vNorm - maxError) / tNorm);
      maxPossibleA = min(maxPossibleA, (vNorm + maxError) / tNorm);
      lastTimeRaw = tRaw;
      lastValueRaw = vRaw;
      currentA = A;
      _addPointToList(tRaw, vRaw);
      return;
    }

    /// If the new model does not fit all the points, we need a new interpolation:
    /// we need to find the "best" turning point.
    ///
    /// We will go through the last inserted points (FIFO order) to make it the
    /// new turning point.
    /// We compute two values currentAE and newAE corresponding to the AEs (AE
    /// stands for Absolute Error, not the average (MAE)) of the two model
    /// characterized by the current turning point (tTP).
    /// We stop once the currentAE+newAE sum stops decreasing OR if the new model
    /// results in errors higher than maxError.
    ///
    /// We separate the list in two parts, one for the old model, one for the
    /// new model. We consider every point of the list as a separator between
    /// those two lists. We start by the last inserted point, and we "go back"
    /// through the list until we have considered them all.
    ///
    /// We have:
    /// listPointsRaw = listCurrent + [tupleTP2] + listNew
    ///
    /// We take the tupleTP2 point from the listCurrent, consider it as the
    /// last point of the currentModel and the startingPoint of the new one,
    /// and then add it to listNew and repeat the process until the end.

    /// We need to have two list sto store the elements of both models.
    List<Tuple2<double, double>> listCurrent = listPointsRaw.toList();
    List<Tuple2<double, double>> listNew = [];

    /// We initialize the MAE sum to the one of the current model with all points.
    double sumAE = _getAE(currentStartingTimeRaw, currentStartingValueRaw,
        lastTimeRaw, lastValueRaw, listCurrent);
    double currentAE;
    double newAE;

    /// We need to keep the maximal and minimal values for the new model's A.
    double newModelA;
    double newModelMinA = -double.maxFinite;
    double newModelMaxA = double.maxFinite;
    double newVNorm;
    double newTNorm;

    /// The list follows the chronological order, we need to reverse it to have
    /// FIFO.
    for (var tuple2TP in listPointsRaw.toList().reversed) {
      listCurrent.removeLast();

      /// We remove tuple2TP from current model.
      newModelA = (tuple2TP.item2 - vRaw) / (tuple2TP.item1 - tRaw);

      /// We check if the new model has an acceptable A.
      if (newModelA > newModelMaxA || newModelA < newModelMinA) {
        /// If not, the previous point was good.
        /// We update the AE to fail the next condition.
        newAE = 1;
        currentAE = 1;
        sumAE = 0;
      } else {
        /// Otherwise, we compute the AEs and update the possible values of A.
        currentAE = _getAE(currentStartingTimeRaw, currentStartingValueRaw,
            tuple2TP.item1, tuple2TP.item2, listCurrent);

        /// We do not add tuple2TP to listNew yet, as it is the new starting
        /// point of the new model.
        newAE = _getAE(tuple2TP.item1, tuple2TP.item2, tRaw, vRaw, listNew);

        /// Updating possible values of newModelA:
        newTNorm = (tuple2TP.item1 - tRaw);
        newVNorm = (tuple2TP.item2 - vRaw);
        newModelMinA = max(newModelMinA, (newVNorm - maxError) / newTNorm);
        newModelMaxA = min(newModelMaxA, (newVNorm + maxError) / newTNorm);
      }
      if (newAE + currentAE > sumAE) {
        /// The previous point was the best local turning point, either:
        /// 1) because the new one incurs error higher than maxError;
        /// 2) because it increases the AEs.
        ///
        /// We update the models.
        if (listNew.isEmpty) {
          /// If the list is empty, the best model is the one with
          /// lastTime/lastValue.
          /// We store the current values, and start the new one.
          listOldModels.add(Tuple3(
              currentStartingTimeRaw, currentStartingValueRaw, currentA));
          currentStartingTimeRaw = lastTimeRaw;
          currentStartingValueRaw = lastValueRaw;
          // currentA = (vRaw - currentStartingValueRaw)/(tRaw-currentStartingTimeRaw);
          tNorm = tRaw - currentStartingTimeRaw;
          vNorm = vRaw - currentStartingValueRaw;
          currentA = vNorm / tNorm;
          minPossibleA = (vNorm - maxError) / tNorm;

          /// There are no other points so no max.
          maxPossibleA = (vNorm + maxError) / tNorm;

          /// Idem.
          lastTimeRaw = tRaw;
          lastValueRaw = vRaw;
          listPointsRaw.clear();
          listPointsRaw.add(Tuple2(tRaw, vRaw));
          return;
        }

        /// The new turning point is the last added point to listNew.
        currentA = (listNew.last.item2 - currentStartingValueRaw) /
            (listNew.last.item1 - currentStartingTimeRaw);
        listOldModels.add(
            Tuple3(currentStartingTimeRaw, currentStartingValueRaw, currentA));
        currentStartingTimeRaw = listNew.last.item1;
        currentStartingValueRaw = listNew.last.item2;

        /// We remove the last element of listNew as it is our starting point
        /// for the new model.
        listNew.removeLast();
        tNorm = tRaw - currentStartingTimeRaw;
        vNorm = vRaw - currentStartingValueRaw;
        currentA = vNorm / tNorm;
        minPossibleA = (vNorm - maxError) / tNorm;

        /// We need to initialize the value.
        maxPossibleA = (vNorm + maxError) / tNorm;

        /// Idem.
        lastTimeRaw = tRaw;
        lastValueRaw = vRaw;

        /// We clear the list of points, and we add the others while
        /// updating the min/max A.
        ///
        ///        listPointsRaw.clear();
        /*
        if (listNew.isEmpty) {
          /// There are only two points, starting
          listPointsRaw.clear();
          minPossibleA = (vNorm-maxError)/tNorm; /// There are no other points so no max
          maxPossibleA = (vNorm+maxError)/tNorm; /// idem
        }
        */
        listNew.add(Tuple2(tRaw, vRaw));

        /// Reverse as the points were added in reversed order.
        _updateList(listNew.reversed.toList());
        return;
      }
      sumAE = currentAE + newAE;

      /// We update the sum of the MAE.
      listNew.add(tuple2TP);

      /// We add the current point to the new model.
    }

    /// We used all the points in memory, we cannot do better.
    /// We update the model.
    currentA = (listPointsRaw.first.item2 - currentStartingValueRaw) /
        (listPointsRaw.first.item1 - currentStartingTimeRaw);
    listOldModels
        .add(Tuple3(currentStartingTimeRaw, currentStartingValueRaw, currentA));

    /// The new interpolation starts at the first points of the list.
    currentStartingTimeRaw = listPointsRaw.first.item1;
    currentStartingValueRaw = listPointsRaw.first.item2;
    tNorm = tRaw - listPointsRaw.first.item1;
    vNorm = vRaw - listPointsRaw.first.item2;
    currentA = vNorm / tNorm;
    listPointsRaw.removeFirst();

    /// We remove the current point.
    listPointsRaw.add(Tuple2(tRaw, vRaw));

    /// No verification, should be good in terms of space.
    _updateList(listPointsRaw.toList());
    lastTimeRaw = tRaw;
    lastValueRaw = vRaw;
    return;
  }

  /// We assume that the list is in the right order.
  void _updateList(List<Tuple2<double, double>> list) {
    listPointsRaw.clear();
    double vNorm_;
    double tNorm_;
    for (var tuple2 in list) {
      listPointsRaw.add(tuple2);
      tNorm_ = tuple2.item1 - currentStartingTimeRaw;
      vNorm_ = tuple2.item2 - currentStartingValueRaw;
      minPossibleA = max(minPossibleA, (vNorm_ - maxError) / tNorm_);
      maxPossibleA = min(maxPossibleA, (vNorm_ + maxError) / tNorm_);
    }
    return;
  }

  static double _getAE(double tStartRaw, double vStartRaw, double tEndRaw,
      double vEndRaw, List<Tuple2<double, double>> listPoints) {
    double A = (vEndRaw - vStartRaw) / (tEndRaw - tStartRaw);
    double mae = 0.0;
    for (var tuple2 in listPoints) {
      mae = mae +
          (A * (tuple2.item1 - tStartRaw) + vStartRaw - tuple2.item2).abs();
    }
    return mae;
  }

  void _addPointToList(double tRaw, double vRaw) {
    listPointsRaw.add(Tuple2(tRaw, vRaw));
    while (listPointsRaw.length > windowSize) {
      listPointsRaw.removeFirst();
    }
  }

  void _initModel(double tRaw, double vRaw) {
    currentStartingTimeRaw = tRaw;
    currentStartingValueRaw = vRaw;
    currentA = 0;
    maxPossibleA = double.maxFinite;
    minPossibleA = -double.maxFinite;
    lastTimeRaw = tRaw;
    lastValueRaw = vRaw;
    listPointsRaw.clear();
  }

  @override
  int getSize() {
    return (super.getSize() + 1 + listPointsRaw.length * 2);
  }

  @override
  String getType() {
    return "FLI Sliding Window";
  }

  @override
  void setParameters(Map<String, String> map) {
    map.forEach((key, value) {
      switch (key) {
        case "error":
          {
            maxError = double.parse(value);
          }
          break;
        case "windowSize":
          {
            windowSize = int.parse(value);
          }
          break;
      }
    });
  }
}
