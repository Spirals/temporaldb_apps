library temporaldb;

import 'package:scidart/numdart.dart';
import 'package:tuple/tuple.dart';
import 'temporal_model.dart';
import 'package:temporaldb/models/greycat.dart';

class SketchDB extends Greycat implements TemporalModel {
  /// Variant of [Greycat] that adds a condition to keep the current model.
  ///
  /// If the current model is stable, i.e. it represents enough points, it is
  /// kept.
  ///
  /// Old params:
  /// Timestamp (non normalized) of the first inserted point
  /// It is used to know if a read is out of bounds
  /// double firstStartingTime=-1;
  ///
  /// Values used for normalisation
  /// double timeValueNormalization = pow(2,22).toDouble();
  /// double yValueNormalization=0;
  ///
  /// Default value for all models
  /// int maxDegree = 14; /// maximum value for the degree of the polynomial regression (PolyFit)
  /// double error=0.01; /// Tolerated error
  ///
  /// Models
  /// Each model has a starting time and a polynomial regression
  /// List<Tuple2<double, PolyFit>> listOldModels = [];
  ///
  /// PolyFit currentModel= PolyFit(Array([0]),Array([0]),0);
  /// double currentStartingTime=0; /// Starting time of the currentModel
  /// double currentTime = 0; /// Last addition => used to regenerate the points used for modeling

  /// Number of points for a model to be kept.
  int numberPointsStabilityModel = 100;

  /// Number of points fitting in the currentModel.
  int numberOfPoints = 0;

  SketchDB([double error_ = 0.01]) {
    error = error_;
  }

  @override
  void setParameters(Map<String, String> map) {
    map.forEach((key, value) {
      switch (key) {
        case "timeValueNormalization":
          {
            timeValueNormalization = double.parse(value);
          }
          break;
        case "yValueNormalization":
          {
            yValueNormalization = double.parse(value);
          }
          break;
        case "error":
          {
            error = double.parse(value);
          }
          break;
        case "maxDegree":
          {
            maxDegree = int.parse(value);
          }
          break;
        case "minNbPointsKeepModel":
          {
            numberPointsStabilityModel = int.parse(value);
          }
          break;
      }
    });
  }

  @override
  void add(double tRaw, double vRaw) {
    /// In all the cases, the currentTime will be tRaw.
    currentTime = tRaw;

    double vNorm = vRaw;
    if (yValueNormalization != 0) {
      vNorm = vRaw / yValueNormalization;
    }

    if (firstStartingTime == -1) {
      setNewTimeAndModel(tRaw, vNorm);
      firstStartingTime = tRaw;
      return;
    }

    var tNorm = (tRaw - currentStartingTime) / timeValueNormalization;

    if (isPredictionCorrect(currentModel, tNorm, vNorm)) {
      numberOfPoints++;
      return;
    }

    if (keepModel()) {
      // debugPrint('Model fitting long enough, storing it.');
      listOldModels.add(Tuple2(currentStartingTime, currentModel));
      setNewTimeAndModel(tRaw, vNorm);
      return;
    }

    /// In the case where the value doesn't fit in the model, we start at the
    /// same degree to see if another would fit better.
    int degree = currentModel.degree;
    PolyFit? temporaryModel;
    /* var points = generatePoints(currentStartingTime, currentTime, degree); //, degree+1, currentModel);
    points.item1.add(tNorm);
    points.item2.add(vNorm);
    temporaryModel = fitPolynom_(Array(points.item1), Array(points.item2), degree);
    if (temporaryModel != null) {
      numberOfPoints++;
      currentModel = temporaryModel;
    }
    else {
      listOldModels.add(Tuple2(currentStartingTime, currentModel));
      setNewTimeAndModel(tRaw, vNorm);
      return; ///Should not be necessary
    }*/

    temporaryModel = fitPolynomial(tNorm, vNorm, degree);
    if (temporaryModel != null) {
      currentModel = temporaryModel;
    } else {
      /// Should not be necessary.
      listOldModels.add(Tuple2(currentStartingTime, currentModel));
      setNewTimeAndModel(tRaw, vNorm);
      return;
    }
  }

  bool keepModel() {
    return (numberPointsStabilityModel <= numberOfPoints);
  }

  @override
  void setNewTimeAndModel(double tRaw, double vNorm) {
    super.setNewTimeAndModel(tRaw, vNorm);
    numberOfPoints = 1;
  }

  /// Returns the count of 64-bits values in the total model.
  @override
  int getSize() {
    return super.getSize() + 2;

    /// SketchDB has only 2 more variables than Greycat
  }

  @override
  String getType() {
    return "SketchingDB";
  }
}
