# FLI example: storing the accelerometer values

This application uses FLI models to store the accelerometer values.
The goal is to show how much memory we can save by using FLI.

## Settings

There are two interesting settings:
- when the device is moving
- when the device does not move

The values returned by the accelerometer are not constant, even when the device is not moving.
By accepting the model to make a given error on the values, we can get rid of the noise made by 
the sensor and have a constant value.

## Getting started

To avoid making phones heat too much, this example app have default time and database insertions 
limits in place. 
If you want the app to run longer, you can edit `_timeLimit` and `_insertionsLimit` variables.