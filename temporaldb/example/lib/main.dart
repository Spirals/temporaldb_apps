import 'dart:async';

import 'package:flutter/material.dart';
import 'package:temporaldb/platform.dart';
import 'package:wakelock/wakelock.dart';
import 'dart:io';
import 'package:flutter_sensors/flutter_sensors.dart';

import 'package:temporaldb/accelerometer_db.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Temporal database demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: const MyHomePage(title: 'Temporal database demo'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key, required this.title}) : super(key: key);
  final String title;
  @override
  State<MyHomePage> createState() => _MyHomePageState();
}


class _MyHomePageState extends State<MyHomePage> {

  @override
  void initState() {
    super.initState();
    /// The following line will enable the Android and iOS wakelock.
    Wakelock.enable();
    /// Initialize accelerometer events stream.
    SensorManager().sensorUpdates(
      sensorId: Sensors.ACCELEROMETER,
      interval: Sensors.SENSOR_DELAY_FASTEST,
    ).then((value) => _stream = value);
  }


  /// The variable _operation is used to prevent to launch two experiments
  /// at the same time. For simplicity we do not use token and assume sequential
  /// accesses to the variable. Values:
  /// 0 : default value, no operations are currently done
  /// 1 : recording
  /// -1 : stop recording
  int _operation = 0;
  String _getNameXP(int nbOperation) {
    switch(nbOperation) {
      case -1: return 'Interrupting experiment...';
      case 0: return 'No experiment running';
      case 1: return 'Recording (mobile) accelerometer values...';
      case 2: return 'Recording (immobile) accelerometer values...';
      default: return 'Error on operation number';
    }
  }
  /// Default number of insertions between two measurements
  static const _stepMeasurement = 100;
  /// Stream used to listen to the accelerometer
  late Stream<SensorEvent> _stream;
  late StreamSubscription<SensorEvent> _subscription;

  /// Sets a limit after which experiment will stop.
  final int _insertionsLimit = 10000;
  final Duration _timeLimit = const Duration(minutes: 2);
  final Stopwatch _stopwatch = Stopwatch();

  /// Number of triplets of accelerometer stored.
  int _numberInsertedTriplets = 0;
  /// Model in which the data is stored
  AccelerometerDB _model = AccelerometerDB();
  /// Size of the model used to store the values.
  int _sizeModelStep = 0;
  /// Number of triplets of accelerometer stored.
  int _numberInsertedTripletsStep = 0;
  /// Variable used to store the timestamp in the experiments
  double _timestamp=0;
  /// File used to store the results: not final since it may change depending
  /// on the experiment or the initial timestamp
  late File outputFile;
  bool _reset=true;

  /// Last values of the accelerometers: used to be printed in the app
  double _accValueX = 0;
  double _accValueY = 0;
  double _accValueZ = 0;

  /// Directory where the results are stored
  static const _addResults = 'results/';



  String _getNameResultsFile(String xp) {
    return '$xp--${getTimestamp()}.txt';
  }

  void _stop() {
    _operation = -1;
    _subscription.cancel();
    _stopwatch.stop();
    setState(() {});
    _operation = 0;
    _mobility='';
  }

  String _mobility = '';

  void _launchMobile() {
    if(!_reset || _operation != 0) {
      return;
    }
    _mobility = 'mobile';
    _operation = 1;
    _launch();
  }
  void _launchImmobile() {
    if(!_reset || _operation != 0) {
      return;
    }
    _mobility = 'immobile';
    _operation = 2;
    _launch();
  }

  void _launch() async {
    _stopwatch.start();

    _reset=false;
    ///Opening file for writing results
    String resultsFileName = _getNameResultsFile('accelerometer');
    outputFile = await localDirFile('${_addResults}accelerometer_$_mobility/',resultsFileName);
    outputFile.open(mode: FileMode.writeOnlyAppend);
    outputFile.writeAsString("NbValues\tRawSize\tModelSize\tGain\tTime\n");

    Future.delayed(_timeLimit, () {
      if (!_reset) {
        _stop();
      }
    });

    double gain;
    double memoryRaw;
    _subscription = _stream.listen((event) {
      setState(() {
        _accValueX = event.data[0];
        _accValueY = event.data[1];
        _accValueZ = event.data[2];
      });

      _timestamp = (DateTime.now().millisecondsSinceEpoch).toDouble();
      _model.add(_timestamp,_accValueX,_accValueY,_accValueZ);
      _numberInsertedTriplets++;

      if (_numberInsertedTriplets >= _insertionsLimit) {
        _stop();
        return;
      }

      if (_numberInsertedTriplets % _stepMeasurement == 0) {
        _sizeModelStep = _model.getSize();
        _numberInsertedTripletsStep = _numberInsertedTriplets;
        memoryRaw = _numberInsertedTripletsStep * 3;
        gain = memoryRaw / _sizeModelStep;
        outputFile.writeAsString("$_numberInsertedTripletsStep\t$memoryRaw\t$_sizeModelStep\t$gain\t$_timestamp\n",mode: FileMode.append);
      }
    });
  }


  void _resetValues(){
    if(_operation != 0) {
      return;
    }
    _model = AccelerometerDB();
    _timestamp=0;
    _numberInsertedTriplets=0;
    _numberInsertedTripletsStep=0;
    _sizeModelStep=0;
    _accValueX=0;
    _accValueY=0;
    _accValueZ=0;
    _mobility='';
    _reset=true;
    _stopwatch.reset();
    setState((){});
  }

  @override
  Widget build(BuildContext context) {
    /// Since stopwatch may be stopped a few moments after time limit has been
    /// reached, timePercentage might be superior to 100.
    int timePercentage = ((_stopwatch.elapsedMilliseconds/_timeLimit.inMilliseconds)*100).toInt();
    const TextStyle btnTxtStyle = TextStyle(color: Colors.white);

    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Container(
        margin: const EdgeInsets.all(20),
        child: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              /// Function used to test new features
              ElevatedButton(
                style: ElevatedButton.styleFrom(
                  backgroundColor: Colors.blue,
                ),
                onPressed: _resetValues,
                child: const Text('Reset values', style: btnTxtStyle),
              ),
              /// Launch XP: storing accelerometer values; immobile case
              ElevatedButton(
                style: ElevatedButton.styleFrom(
                  backgroundColor: Colors.blue,
                ),
                onPressed: _launchImmobile,
                child: const Text('Launch XP: no movement', style: btnTxtStyle),
              ),
              /// Launch XP: storing accelerometer values; mobile case
              ElevatedButton(
                style: ElevatedButton.styleFrom(
                  backgroundColor: Colors.blue,
                ),
                onPressed: _launchMobile,
                child: const Text('Launch XP: move', style: btnTxtStyle),
              ),
              Container(
                margin: const EdgeInsets.symmetric(vertical: 30),
                child: Text(
                  _getNameXP(_operation),
                  style: Theme.of(context).textTheme.titleLarge,
                  textAlign: TextAlign.center,
                ),
              ),
              Text(
                  'Values of the accelerometer:',
                  style: Theme.of(context).textTheme.titleLarge
              ),
              Text( 'x=$_accValueX' ),
              Text( 'y=$_accValueY' ),
              Text( 'z=$_accValueZ' ),
              const Divider(),
              Container(
                margin: const EdgeInsets.symmetric(vertical: 10),
                child: Column(
                  children: [
                    Text(
                      'Number of inserted triplets: $_numberInsertedTriplets',
                      style: Theme.of(context).textTheme.titleLarge,
                    ),
                    Text(
                        "Target count: $_insertionsLimit (${((_numberInsertedTriplets/_insertionsLimit)*100).toInt()}%)",
                      style: TextStyle(color: _numberInsertedTriplets == _insertionsLimit ? Colors.lightGreen : Colors.black),
                    ),
                    Container(
                      margin: const EdgeInsets.only(top: 5),
                      child: Text(
                        "Current duration: ${_stopwatch.elapsed.toString().split('.').first.padLeft(8, "0")}",
                        style: Theme.of(context).textTheme.titleLarge,
                      ),
                    ),
                    Text(
                        "Duration limit: ${_timeLimit.toString().split('.').first.padLeft(8, "0")} (${timePercentage > 100 ? "100" : timePercentage}%)",
                      style: TextStyle(color: timePercentage >= 100 ? Colors.lightGreen : Colors.black),
                    ),
                  ],
                ),
              ),
              Text(
                'Size of the model:',
                style: Theme.of(context).textTheme.titleLarge,
              ),
              Text( '$_sizeModelStep bits instead of ${_numberInsertedTripletsStep*3}' ),
              Container(
                margin: const EdgeInsets.only(top: 10),
                child: Text(
                  'Gain:',
                  style: Theme.of(context).textTheme.titleLarge,
                ),
              ),
              Text( _sizeModelStep == 0 ? 'None' : 'x${((_numberInsertedTripletsStep*3).toDouble()/(_sizeModelStep).toDouble()).toStringAsFixed(3)}' ),
            ],
          ),
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: _stop,
        tooltip: 'Interrupt experiment',
        backgroundColor: Colors.red,
        child: const Icon(Icons.clear_rounded),
      ), // This trailing comma makes auto-formatting nicer for build methods.
    );
  }
}
