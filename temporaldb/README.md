# Temporal database package

temporaldb is an experimental library for flutter.

Its main purpose is to provide temporal databases to allow mobile devices able to store an important
quantity of data. By using models, this quantity is potentially unlimited.

## MODELS

In order to store efficiently temporal data, the library includes several modeling techniques:
- Fast Linear Interpolation (FLI)
- Sliding Windows And Bottom-up (SWAB)
- Greycat

Not only we are the first to port SWAB and Greycat to mobile devices, we propose FLI, a storage
system based on a fast PLA to model temporal data.

## Getting started
To use a given model you can use the model directly (in ./models/) or use the Factory class in
temporalDB_1D_factory.dart
The default type is FLAIR (value 0), otherwise:
- 1 for SWAB
- 2 for Greycat
