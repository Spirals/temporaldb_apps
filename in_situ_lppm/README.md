# in_situ_lppm

This application is made to show that the use of FLI enables the full storage of GPS databases
and the use of POI attacks and LPPMs directly on the phone.

It has been tested on Android and iOS.

## Purpose of this application

This app presents five buttons, one for each following experiment:
- Loading the full cabspotting database in memory. Each user trace is modeled using FLI, then
reconstructed; POI attacks and Promesse are then used on both raw and modeled traces.
The cumulative distribution of gain, memory-wise, and of the spatial errors of POI are stored. 
- The same experiment is done with different error thresholds.
- Loading the full Cabspotting database in memory and computing POIs with and without our new approach;
The cumulative distribution of distances are stored. Note that FLI is not used in this experiment.
- Loading the trace of the user 1 of the Privamov dataset. Comparing the computation time of the POIs
with and without our new approach; modeling the trace and computing POIs on it.
- Loading the full Privamov dataset. Comparing the sizes with and without FLI.

The last button, at the bottom right of the application, interrupts the current experiment (does not
work on the user 1 experiment).

#### Access result files

For each experiment, a file is created in the local directory of the app.

###### Android

For Android, files are located at:

`/storage/emulated/0/Android/data/fr.inria.spirals.temporaldb.in_situ_lppm/files/`

You can retrieve them using `adb` or Android Studio's device file explorer.

###### iOS

On iOS, you have to export data from application before reading it:

1. In Xcode, open `Window > Devices and Simulators`, and find tested application under 
`Installed apps` under your phone's menu
2. Export application container by selecting tested application and clicking `Download container`
option in the menu below
3. `cd` your way to the `results` directory, which is located in 
`[YOUR_CONTAINER_NAME]/AppData/Library/Application\ Support`

## Getting Started

This project requires FLI, included in the package `temporaldb`.
Clone the `temporaldb` project and update its path in the pubspec.yaml file.
