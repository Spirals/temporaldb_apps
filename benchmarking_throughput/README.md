# benchmarking_throughput

Application made for benchmarking FLI against SWAB and Greycat by comparing their throughput (write
and read).
The application runs on both Android and iOS.

## Purpose of this application

This app has six main buttons, one for each following experiment:
- Inserting random values in FLI.
- Inserting random values in SWAB.
- Inserting random values in Greycat.
- Reading values in FLI.
- Reading values in SWAB.
- Reading values in Greycat.

#### Access result files

For each experiment, a file is created in the local directory of the app.
By default, 1,000,000 writes/reads are performed. The total time is written in the results file.

###### Android

For Android, files are located at:

`/data/data/fr.inria.spirals.temporaldb.benchmarking_throughput/files/results/`

You can retrieve them using `adb` or Android Studio's device file explorer.

###### iOS

On iOS, you have to export data from application before reading it:

1. In Xcode, open `Window > Devices and Simulators`, and find tested application under `Installed apps`
   under your phone's menu
2. Export application container by selecting tested application and clicking `Download container` option
   in the menu below
3. `cd` your way to the `results` directory, which is located in `[YOUR_CONTAINER_NAME]/AppData/Library/Application\ Support`

## Getting Started

This project requires FLI, included in the package `temporaldb`.

Clone the `temporaldb` project and update its path in the pubspec.yaml file.
