import 'package:flutter/material.dart';
import 'dart:math';

import 'package:temporaldb/platform.dart';
import 'package:tuple/tuple.dart';
import 'package:wakelock/wakelock.dart';

import 'package:temporaldb/temporal_db_1d_factory.dart';
import 'package:temporaldb/models/temporal_model.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: const MyHomePage(title: 'Benchmarking throughput'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key, required this.title}) : super(key: key);
  final String title;
  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  /// The variable operation is used to prevent to launch two experiments
  /// at the same time. For simplicity we do not use token and assume sequential
  /// accesses to the variable. Values:
  /// -1 : interrupting the ongoing experiment
  /// 0 : default value, no operations are currently done
  /// 1 : XP on sqlite on random values
  /// 2 : FLI on random values
  /// 3 : SQLite on constant values
  /// 4 : FLI on constant values
  int _operation = 0;
  String _getNameXP(int nbOperation) {
    switch (nbOperation) {
      case -1:
        return 'Interrupting experiment...';
      case 0:
        return 'No experiment running';
      case 1:
        return 'Benchmarking insertions with FLI';
      case 2:
        return 'Benchmarking insertions with SWAB';
      case 3:
        return 'Benchmarking insertions with Greycat';
      case 4:
        return 'Benchmarking reads with FLI';
      case 5:
        return 'Benchmarking reads with SWAB';
      case 6:
        return 'Benchmarking reads with Greycat';
      default:
        return 'Error on operation number';
    }
  }

  /// Directory where the results are stored
  static const _addResults = './results/';

  /// Default number of insertions for each experiment
  static const _numberInsertions = 1000000;

  /// Default number of insertions for each experiment of Greycat
  static const _numberInsertionsGreycat = 10000;

  /// Default number of reads for each experiment
  static const _numberReads = 10000;

  @override
  void initState() {
    super.initState();

    /// The following line will enable the Android and iOS wakelock.
    Wakelock.enable();
  }

  void _printResults(String relativeAdd, String results) async {
    var file = await localDirFile(relativeAdd, 'results-${getTimestamp()}.txt');
    // debugPrint(file.absolute.path);
    file.writeAsStringSync(results);
    return;
  }

  void _interruptXP() {
    _operation = -1;
    setState(() {});
  }

  int _doInsertionXP(TemporalModel db, int numberInsertions) {
    Random rd = Random(0);
    int timestamp;

    Stopwatch stopwatch = Stopwatch()..start();
    int i;
    for (i = 0; i < numberInsertions; i++) {
      timestamp = DateTime.now().microsecondsSinceEpoch;
      db.add(timestamp.toDouble(), getNextDouble(rd));
    }
    // stopwatch.stop();
    return (stopwatch.elapsedMilliseconds);
  }

  List<Tuple2<double, double>> _generatePoints(int numberPoints) {
    Random rd = Random(0);
    double x = 0;
    double y;
    List<Tuple2<double, double>> points = [];
    for (int i = 0; i < numberPoints; i++) {
      x = x + getNextPositiveDouble(rd) + 0.1;
      y = getNextDouble(rd);
      points.add(Tuple2(x, y));
    }
    return points;
  }

  List<Tuple2<double, double>> _getSubPoints(
      List<Tuple2<double, double>> listPoints, int numberReads) {
    Random rd = Random(100);
    int maxRd = listPoints.length;
    List<Tuple2<double, double>> subList = [];
    for (int i = 0; i < numberReads; i++) {
      subList.add(listPoints[rd.nextInt(maxRd)]);
    }
    return subList;
  }

  int _doInsertions(TemporalModel db, List<Tuple2<double, double>> points) {
    Tuple2<double, double> pair;
    Stopwatch stopwatch = Stopwatch()..start();
    for (int i = 0; i < points.length; i++) {
      pair = points[i];
      // for(var pair in points) {
      db.add(pair.item1, pair.item2);
      // if (i%100==0) {
      //   // if (db.getType() == 'Greycat') {
      //   debugPrint('insertion $i done!');
      // }
      if (_operation == -1) {
        debugPrint('Stopping insertions at point $i');
        break;
      }
    }
    return stopwatch.elapsedMilliseconds;
  }

  int _doReads(TemporalModel db, List<Tuple2<double, double>> points) {
    Tuple2<double, double> pair;
    Stopwatch stopwatch = Stopwatch()..start();
    // for(var pair in points) {
    for (int i = 0; i < points.length; i++) {
      pair = points[i];
      db.read(pair.item1);
      // if (i%100==0) {
      //   // debugPrint('Read $i done!');
      //   if(_operation==-1) {
      //     break;
      //   }
      // }
    }
    return stopwatch.elapsedMilliseconds;
  }

  Future<int> _doReadXP(
      TemporalModel db, int numberInsertions, int numberReads) async {
    debugPrint('Starting generating points');
    List<Tuple2<double, double>> points = _generatePoints(numberInsertions);
    debugPrint('Generation of points done');
    await Future.delayed(const Duration(milliseconds: 500));
    int time = _doInsertions(db, points);
    debugPrint('Insertions done in $time ms');
    debugPrint('Number of models: ${db.getNbModels()}');
    // Stopwatch stopwatch = Stopwatch()..start();
    if (numberInsertions == numberReads) {
      time = _doReads(db, points);
    } else {
      time = _doReads(db, _getSubPoints(points, numberReads));
    }
    debugPrint('Reads done in $time ms');
    // return (stopwatch.elapsedMilliseconds);
    return time;
  }

  void _doInsertionsFLIXP() async {
    debugPrint('Starting insertions with FLI');

    /// Updating the variable _operation to prevent another xp to be launched
    if (_operation != 0) {
      return;
    }
    _operation = 1;
    setState(() {});
    await Future.delayed(const Duration(milliseconds: 500));

    TemporalModel db = Factory();
    int elapsedTimeMilliseconds = _doInsertionXP(db, _numberInsertions);

    String results =
        '{"DB":"FLI","nbInsertions":$_numberInsertions,"elapsedTimeMilliseconds":$elapsedTimeMilliseconds}';
    _printResults('${_addResults}InsertionFLI', results);

    _operation = 0;
    setState(() {});
    await Future.delayed(const Duration(milliseconds: 500));
    debugPrint('Done in $elapsedTimeMilliseconds');
    debugPrint('Insertions with FLI: done');
  }

  void _doInsertionsSWABXP() async {
    debugPrint('Starting insertions with SWAB');

    /// Updating the variable _operation to prevent another xp to be launched
    if (_operation != 0) {
      return;
    }
    _operation = 2;
    setState(() {});
    await Future.delayed(const Duration(milliseconds: 500));
    TemporalModel db = Factory(1);
    int elapsedTimeMilliseconds = _doInsertionXP(db, _numberInsertions);
    String results =
        '{"DB":"SWAB","nbInsertions":$_numberInsertions,"elapsedTimeMilliseconds":$elapsedTimeMilliseconds}';
    _printResults('${_addResults}InsertionSWAB', results);
    _operation = 0;
    setState(() {});
    await Future.delayed(const Duration(milliseconds: 500));
    debugPrint('Done in $elapsedTimeMilliseconds');
    debugPrint('Insertions with SWAB: done');
  }

  void _doInsertionsGreycatXP() async {
    debugPrint('Starting insertions with Greycat');

    /// Updating the variable _operation to prevent another xp to be launched
    if (_operation != 0) {
      return;
    }
    _operation = 3;
    setState(() {});
    await Future.delayed(const Duration(milliseconds: 500));
    TemporalModel db = Factory(2);
    int elapsedTimeMilliseconds = _doInsertionXP(db, _numberInsertionsGreycat);
    String results =
        '{"DB":"Greycat","nbInsertions":$_numberInsertionsGreycat,"elapsedTimeMilliseconds":$elapsedTimeMilliseconds}';
    _printResults('${_addResults}InsertionGreycat', results);
    _operation = 0;
    setState(() {});
    await Future.delayed(const Duration(milliseconds: 500));
    debugPrint('Done in $elapsedTimeMilliseconds');
    debugPrint('Insertions with Greycat: done');
  }

  void _doReadsFLIXP() async {
    debugPrint('Starting reads with FLI');

    /// Updating the variable _operation to prevent another xp to be launched
    if (_operation != 0) {
      return;
    }
    _operation = 4;
    setState(() {});
    await Future.delayed(const Duration(milliseconds: 500));

    TemporalModel db = Factory();
    int elapsedTimeMilliseconds =
        await _doReadXP(db, _numberInsertions, _numberReads);

    String results =
        '{"DB":"FLI","nbInsertions":$_numberInsertions,"nbReads":$_numberReads,"elapsedTimeMilliseconds":$elapsedTimeMilliseconds}';
    _printResults('${_addResults}ReadFLI', results);

    _operation = 0;
    setState(() {});
    await Future.delayed(const Duration(milliseconds: 500));
    debugPrint('Done in $elapsedTimeMilliseconds');
    debugPrint('Reads with FLI: done');
  }

  void _doReadsSWABXP() async {
    debugPrint('Starting reads with SWAB');

    /// Updating the variable _operation to prevent another xp to be launched
    if (_operation != 0) {
      return;
    }
    _operation = 5;
    setState(() {});
    await Future.delayed(const Duration(milliseconds: 500));
    TemporalModel db = Factory(1);
    int elapsedTimeMilliseconds =
        await _doReadXP(db, _numberInsertions, _numberReads);
    String results =
        '{"DB":"SWAB","nbInsertions":$_numberInsertions,"nbReads":$_numberReads,"elapsedTimeMilliseconds":$elapsedTimeMilliseconds}';
    _printResults('${_addResults}ReadSWAB', results);
    _operation = 0;
    setState(() {});
    await Future.delayed(const Duration(milliseconds: 500));
    debugPrint('Done in $elapsedTimeMilliseconds');
    debugPrint('Reads with SWAB: done');
  }

  void _doReadsGreycatXP() async {
    debugPrint('Starting reads with Greycat');

    /// Updating the variable _operation to prevent another xp to be launched
    if (_operation != 0) {
      return;
    }
    _operation = 6;
    setState(() {});
    await Future.delayed(const Duration(milliseconds: 500));
    TemporalModel db = Factory(2);
    int elapsedTimeMilliseconds =
        await _doReadXP(db, _numberInsertionsGreycat, _numberReads);
    String results =
        '{"DB":"Greycat","nbInsertions":$_numberInsertionsGreycat,"nbReads":$_numberReads,"elapsedTimeMilliseconds":$elapsedTimeMilliseconds}';
    _printResults('${_addResults}ReadGreycat', results);
    _operation = 0;
    setState(() {});
    await Future.delayed(const Duration(milliseconds: 500));
    debugPrint('Done in $elapsedTimeMilliseconds');
    debugPrint('Reads with Greycat: done');
  }

  @override
  Widget build(BuildContext context) {
    const TextStyle btnTxtStyle = TextStyle(color: Colors.white);

    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            const Text(
              'Choose the experiment:',
            ),

            /// Launch XP: benchmarking insertions with FLI
            ElevatedButton(
              style: ElevatedButton.styleFrom(
                backgroundColor: Colors.blue,
              ),
              onPressed: _doInsertionsFLIXP,
              child: const Text(
                'XP: benchmarking insertions with FLI',
                style: btnTxtStyle,
              ),
            ),

            /// Launch XP: benchmarking insertions with SWAB
            ElevatedButton(
              style: ElevatedButton.styleFrom(
                backgroundColor: Colors.blue,
              ),
              onPressed: _doInsertionsSWABXP,
              child: const Text(
                'XP: benchmarking insertions with SWAB',
                style: btnTxtStyle,
              ),
            ),

            /// Launch XP: benchmarking insertions with Greycat
            ElevatedButton(
              style: ElevatedButton.styleFrom(
                backgroundColor: Colors.blue,
              ),
              onPressed: _doInsertionsGreycatXP,
              child: const Text(
                'XP: benchmarking insertions with Greycat',
                style: btnTxtStyle,
              ),
            ),

            /// Launch XP: benchmarking reads with FLI
            ElevatedButton(
              style: ElevatedButton.styleFrom(
                backgroundColor: Colors.blue,
              ),
              onPressed: _doReadsFLIXP,
              child: const Text(
                'XP: benchmarking reads with FLI',
                style: btnTxtStyle,
              ),
            ),

            /// Launch XP: benchmarking reads with SWAB
            ElevatedButton(
              style: ElevatedButton.styleFrom(
                backgroundColor: Colors.blue,
              ),
              onPressed: _doReadsSWABXP,
              child: const Text(
                'XP: benchmarking reads with SWAB',
                style: btnTxtStyle,
              ),
            ),

            /// Launch XP: benchmarking reads with Greycat
            ElevatedButton(
              style: ElevatedButton.styleFrom(
                backgroundColor: Colors.blue,
              ),
              onPressed: _doReadsGreycatXP,
              child: const Text(
                'XP: benchmarking reads with Greycat',
                style: btnTxtStyle,
              ),
            ),

            Text(
              _getNameXP(_operation),
            ),
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: _interruptXP,
        tooltip: 'Interrupt experiment',
        backgroundColor: Colors.red,
        child: const Icon(
          Icons.clear_rounded,
          color: Colors.white,
        ),
      ), // This trailing comma makes auto-formatting nicer for build methods.
    );
  }
}
